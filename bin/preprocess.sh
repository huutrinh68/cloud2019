mkdir -p .cache

# train
python -m src.preprocess.make_folds --input ./input/understanding_cloud_organization/train.csv --output ./.cache/train_folds.pkl --n-fold 15 --seed 42

# test
python -m src.preprocess.create_dataset --input ./input/understanding_cloud_organization/sample_submission.csv --output ./.cache/test.pkl