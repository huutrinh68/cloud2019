#!/bin/bash

model=model002
mode=train
gpu1=2
conf=./conf/${model}.py
n_tta=3

export CUDA_VISIBLE_DEVICES=2

for fold in `seq 0 14`
do
    python -m src.main002 ${mode} ${conf} --fold ${fold} --gpu ${gpu1} --n_tta ${n_tta}
done