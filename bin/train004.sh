#!/bin/bash

model=model004
mode=train
gpu1=3
conf=./conf/${model}.py
n_tta=3

export CUDA_VISIBLE_DEVICES=3

for fold in `seq 0 14`
do
    python -m src.main004 ${mode} ${conf} --fold ${fold} --gpu ${gpu1} --n_tta ${n_tta}
done