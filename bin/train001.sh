#!/bin/bash

model=model001
mode=train
gpu1=1
conf=./conf/${model}.py
n_tta=3

export CUDA_VISIBLE_DEVICES=1

for fold in `seq 0 14`
do
    python -m src.main001 ${mode} ${conf} --fold ${fold} --gpu ${gpu1} --n_tta ${n_tta}
done