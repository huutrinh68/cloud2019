model=model001
gpu1=1
# gpu2=3
tta=none_hflip_vflip
conf=./conf/${model}.py
note=linknet_resnet152

output=./${model}/test/${model}_${note}_tta${tta}.csv

export CUDA_VISIBLE_DEVICES=0,1,2,3
# create submit file
python -m src.make_submission001 ${conf} --output ${output} --n_tta ${tta} --gpu ${gpu1} #${gpu2}

# submit
# kaggle competitions submit -c understanding_cloud_organization -f ${output} -m ""

