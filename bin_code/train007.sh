#!/usr/bin/env bash
model=model007
config=code/conf/${model}.py
n_tta=3

python -m code.train007 \
${config} \
--folds 0 --folds 1 --folds 2 --folds 3 --folds 4 \
--folds 5 --folds 6 \
--gpu 2
