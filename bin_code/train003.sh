#!/usr/bin/env bash
model=model003
config=code/conf/${model}.py
n_tta=3

python -m code.train003 \
${config} \
--folds 0 --folds 1 --folds 2 --folds 3 --folds 4 \
--folds 5 --folds 6 --folds 7 --folds 8 --folds 9 \
--folds 10 --folds 11 --folds 12 --folds 13 --folds 14 \
--gpu 2
