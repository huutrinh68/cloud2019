Refer Link:  
https://arxiv.org/pdf/1810.00736.pdf  
https://openreview.net/pdf?id=Bygq-H9eg  

| Model | BS | LR <br>decoder-encoder | Epoch | Local:loss/score | LB | Score Type | Note |
| :--- | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Resnet18 | 4*10 | 1e-2 - 1e-3 | 17 | 0.768412/0.5157 | 0.630 | BestLoss | Adam, CV:20%, bestthre=0.6 minsize=20000 |
| Resnet34 | 4*10 | 1e-2 - 1e-3 | 17 | x | 0.608 | BestDice | Adam, CV:20%, bestthre=0.55 minsize=21000 |
| Resnet34 | 6*10 | 5e-3 - 5e-4 | 35 | 0.747443/0.548896 | 0.643 | BestDice | Adam, CV:20%, bestthre=0.55 minsize=21000 |
| Resnet101 | 4*10 | 5e-3 - 5e-4 | 19 | x | 0.640 | BestDice | Adam, CV:20%, bestthre=0.6 minsize=20000 |
| Resnet101 | 4*10 | 5e-3 - 5e-4 | 19 | x | 0.641 | BestLoss | Adam, CV:20%, bestthre=0.6 minsize=20000 |
| Resnet101 | 4*10 | 5e-3 - 5e-4 | 19 | x | 0.646 | BestLoss | Adam, CV:7.5%, bestthre=0.6 minsize=20000 |
| Resnet101 | 4*10 | 5e-3 - 5e-4 | 19 | x | 0.639 | BestDice | Adam, CV:7.5%, bestthre=0.6 minsize=20000 |
| Resnet101 | 4*10 | 5e-3 - 5e-3 | 25 | x | 0.605 | BestDice | Adam, CV:20%, bestthre=0.6 minsize=20000 |
| Resnet101 | 4*10 | 5e-4 - 5e-4 | 25 | 0.788938/0.508645 | 0.635 | BestLoss | Adam, CV:10%, bestthre=0.6 minsize=20000 |
| Resnet101 | 8*10 | 5e-3 - 5e-4 | 24 | 0.765516/0.539409 | 0.645 | BestLoss | Adam, CV:20%, bestthre=0.6 minsize=20000 |
| Resnet101 | 8*10 | 5e-3 - 5e-4 | 13 | 0.772158/0.533310 | 0.639 | BestLoss | Adam, CV:20%, (0,(0.6,25000)),(1,(0.65,25000)),(2,(0.6,25000)),(3,(0.6,10000)) |
| Resnet152 | 4*10 | 5e-3 - 5e-4 | 50 | 0.737329/0.561056 | 0.648 | BestLoss | Adam, CV:15%, bestthre=0.6 minsize=20000|
| Resnet152 | 6*10 | 5e-3 - 5e-4 | 27 | 0.733435/0.555613 | 0.651 | BestLoss | Adam, CV:20%, bestthre=0.6 minsize=20000|
| Se_resnext101_32x4d | 4*10 | 5e-3 - 5e-3 | 19 | 0.829/0.4966 | 0.629 | BestLoss | Adam, CV:20%, bestthre=0.6 minsize=20000 |
| Se_resnext101_32x4d | 4*10 | 5e-3 - 5e-3 | 19 | 0.819479/0.497804 | 0.629 | BestDice | Adam, CV:20%, bestthre=0.6 minsize=20000 |
- [ ] Resnet18
  - [x] Training
  - [ ] Done
- [ ] Resnet34
  - [x] Training
  - [ ] Done
- [ ] Resnet50
  - [ ] Training
  - [ ] Done
- [ ] Resnet101
  - [x] Training
  - [ ] Done
- [ ] Se_resnext101_32x4d
  - [x] Training
  - [ ] Done