from code.utils.logger import log

import os 
import cv2
import pandas as pd 
import numpy as np 
from sklearn.model_selection import train_test_split
import albumentations
from albumentations import pytorch as AT
from torch.utils.data import Dataset, DataLoader
#, WeightedRandomSampler
# from utils import stratified_group_k_fold
from code.utils.split import stratified_group_k_fold
import torch

# import segmentation_models_pytorch as smp

# convert to tensor then move channel to the first
def to_tensor(x, **kwargs):
    return x.transpose(2, 0, 1).astype('float32')

# # https://www.kaggle.com/c/understanding_cloud_organization/discussion/109645?fbclid=IwAR2WbdCDC30lUwCOAsptEXE10T5LB90rCvUrG4to7kiECzvngcFf4LG61QU#latest-633379
# # TODO: set augmentation same to LinhPV
# # get train augmentaion
# # def get_training_augmentation():
# #     train_transform = [
# #         albumentations.Resize(320, 640),
# #         albumentations.VerticalFlip(), # add new then got 0.652
# #         albumentations.HorizontalFlip(),
# #         albumentations.Rotate(limit=30), # add new then got 0.652
# #         albumentations.GridDistortion(),
# #         # albumentations.OpticalDistortion(), # add new to test -> not good: 0.651 with top3
# #         # albumentations.RandomBrightness(limit=0.2, always_apply=False, p=0.5), # add new
# #         # albumentations.RandomContrast(limit=0.2, always_apply=False, p=0.5), # add new
# #         # albumentations.ShiftScaleRotate(scale_limit=0.5, rotate_limit=0, shift_limit=0.1, p=0.5, border_mode=0),
# #         # albumentations.OpticalDistortion(p=0.5, distort_limit=2, shift_limit=0.5),
# #         # albumentations.Blur(blur_limit=5, always_apply=False, p=0.5), # add new
# #         # albumentations.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0, always_apply=False, p=1.0) # add new
# #     ]
# #     return albumentations.Compose(train_transform, p=1)

def get_training_augmentation():
    train_transform = [
        # albumentations.Resize(320, 640),
        # albumentations.Resize(384, 576),
        # albumentations.Resize(768, 1152),
        # albumentations.Resize(640, 960), #model006, model007, model008
        albumentations.Resize(1280, 2048), #model009
        albumentations.VerticalFlip(),
        albumentations.HorizontalFlip(),
        albumentations.GridDistortion(),
        albumentations.OpticalDistortion(),
        # albumentations.RandomBrightnessContrast(),
    ]
    return albumentations.Compose(train_transform, p=1)

# get validation augmetation
def get_validation_augmentation():
    """Add paddings to make image shape divisible by 32"""
    test_transform = [
        # albumentations.Resize(320, 640),
        # albumentations.Resize(384, 576),
        # albumentations.Resize(768, 1152),
        # albumentations.Resize(640, 960), #model006, model007, model008
        albumentations.Resize(1280, 2048), #model009
        # albumentations.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0, always_apply=False, p=1.0) # add new
    ]
    return albumentations.Compose(test_transform)

# # make masks data, which to be labels
def make_mask(df: pd.DataFrame, image_name: str='img.jpg', shape: tuple = (1400, 2100)):
    encoded_masks = df.loc[df['im_id'] == image_name, 'EncodedPixels']
    masks = np.zeros((shape[0], shape[1], 4), dtype=np.float32)

    for idx, label in enumerate(encoded_masks.values):
        if label is not np.nan:
            mask = rle_decode(label)
            masks[:, :, idx] = mask
            
    return masks


# # mask to run length encode
# def mask2rle(img):
#     '''
#     Convert mask to rle.
#     img: numpy array, 1 - mask, 0 - background
#     Returns run length as string formated
#     '''
#     pixels= img.T.flatten()
#     pixels = np.concatenate([[0], pixels, [0]])
#     runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
#     runs[1::2] -= runs[::2]
#     return ' '.join(str(x) for x in runs)

# run length decode
def rle_decode(mask_rle: str = '', shape: tuple = (1400, 2100)):
    '''
    Decode rle encoded mask.
    
    :param mask_rle: run-length as string formatted (start length)
    :param shape: (height, width) of array to return 
    Returns numpy array, 1 - mask, 0 - background
    '''
    s = mask_rle.split()
    starts, lengths = [np.asarray(x, dtype=int) for x in (s[0:][::2], s[1:][::2])]
    starts -= 1
    ends = starts + lengths
    img = np.zeros(shape[0] * shape[1], dtype=np.uint8)
    for lo, hi in zip(starts, ends):
        img[lo:hi] = 1
    return img.reshape(shape, order='F')

# # post processing to remove tiny mask
# def post_process(probability, threshold, min_size):
#     """
#     Post processing of each predicted mask, components with lesser number of pixels
#     than `min_size` are ignored
#     """
#     # don't remember where I saw it
#     mask = cv2.threshold(probability, threshold, 1, cv2.THRESH_BINARY)[1]
#     num_component, component = cv2.connectedComponents(mask.astype(np.uint8))
#     predictions = np.zeros((350, 525), np.float32)
#     num = 0
#     for c in range(1, num_component):
#         p = (component == c)
#         if p.sum() > min_size:
#             predictions[p] = 1
#             num += 1
#     return predictions, num


# get preprocessing
def get_preprocessing(preprocessing_fn): 

    _transform = [
        albumentations.Lambda(image=preprocessing_fn),
        albumentations.Lambda(image=to_tensor, mask=to_tensor),
    ]
    return albumentations.Compose(_transform)

# get train, valid ids
def get_train_valid_ids(fold, cfg):
    # bad image list
    bad_train = ["5e70931.jpg", "7ca1d0b.jpg", "1e40a05.jpg", "8bd81ce.jpg", "41f92e5.jpg", "449b792.jpg", "563fc48.jpg",
             "1588d4c.jpg", "046586a.jpg", "b092cc1.jpg", "c26c635.jpg", "c0306e5.jpg", "e04fea3.jpg", "e5f2f24.jpg",
             "ee0ba55.jpg", "fa645da.jpg", "eda52f2.jpg", "d821c94.jpg", "24884e7.jpg", "d744e88.jpg"]

    cloud2019_train = pd.read_csv(os.path.join(cfg.data_dir, 'train.csv'))

    # debug mode 
    log.info(cfg.mode)
    if cfg.mode == 'debug':
        log.info(f'mode: {cfg.mode}')
        cloud2019_train = cloud2019_train.iloc[:150]
    elif cfg.mode == 'train':
        log.info(f'mode: {cfg.mode}')
    
    cloud2019_train['label'] = cloud2019_train['Image_Label'].apply(lambda x: x.split('_')[1])
    cloud2019_train['im_id'] = cloud2019_train['Image_Label'].apply(lambda x: x.split('_')[0])

    # remove bad image list from train data
    cloud2019_train = cloud2019_train[~cloud2019_train['im_id'].isin(bad_train)]
    log.info(f'removed {len(bad_train)} bad image')

    #https://www.kaggle.com/c/understanding_cloud_organization/discussion/111731#latest-645372
    id_mask_count = cloud2019_train.loc[cloud2019_train['EncodedPixels'].isnull() == False, 'Image_Label'].apply(
        lambda x: x.split('_')[0]).value_counts().reset_index().rename(columns={'index': 'img_id', 'Image_Label': 'count'}).sort_values(['count', 'img_id'])

    # split by id_mask_count
    if fold is not None:
        log.info(f'train ids, valid ids for fold {fold}th in total {len(cfg.folds)} fold')
        # add fold information
        id_mask_count['fold'] = stratified_group_k_fold(
            label='count', group_column='img_id', df=id_mask_count, n_splits=len(cfg.folds)
        )

        # get train_image_id for fold_th 
        train_image = np.setdiff1d(id_mask_count['img_id'], id_mask_count[id_mask_count['fold']==fold]['img_id'])

        train_ids_fold = id_mask_count[id_mask_count['img_id'].isin(train_image)]['img_id'].tolist()
        valid_ids_fold = id_mask_count[~id_mask_count['img_id'].isin(train_image)]['img_id'].tolist()
    else:
        train_ids_fold, valid_ids_fold = train_test_split(id_mask_count['img_id'].values, test_size=0.2, random_state=42, stratify=id_mask_count['count'])

    log.info(f'len(train_ids) = {len(train_ids_fold)}')
    log.info(f'len(valid_ids) = {len(valid_ids_fold)}')

    return cloud2019_train, train_ids_fold, valid_ids_fold

# # get test ids
# def get_test_ids(data_dir):
#     cloud2019_test = pd.read_csv(os.path.join(data_dir, 'sample_submission.csv'))
#     cloud2019_test['label'] = cloud2019_test['Image_Label'].apply(lambda x: x.split('_')[1])
#     cloud2019_test['im_id'] = cloud2019_test['Image_Label'].apply(lambda x: x.split('_')[0])
#     test_ids = cloud2019_test['Image_Label'].apply(lambda x: x.split('_')[0]).drop_duplicates().values

#     return cloud2019_test, test_ids

# get dataloader
def get_dataloader(
    data_dir, 
    df, 
    datatype, 
    img_ids, 
    batch_size, 
    num_workers, 
    transforms, 
    preprocessing):

    dataset = CloudDataset(
        data_dir=data_dir, 
        df=df, 
        datatype=datatype, 
        img_ids=img_ids, 
        transforms = transforms, 
        preprocessing=preprocessing)

    if datatype=='train': 
        shuffle = True
    else:
        shuffle = False  
    loader = DataLoader(dataset, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers)
    return loader


########## dataset ##########
class CloudDataset(Dataset):
    '''
    Dataset for Segmentation task
    '''
    def __init__(
        self, 
        data_dir, 
        df: pd.DataFrame = None, 
        datatype: str = 'train', 
        img_ids: np.array = None,
        transforms = albumentations.Compose([
            albumentations.HorizontalFlip(), 
            AT.ToTensor(),
            ]),
        preprocessing=None):
        self.df = df
        if datatype != 'test':
            self.data_folder = f"{data_dir}/train_images"
        else:
            self.data_folder = f"{data_dir}/test_images"
        self.img_ids = img_ids
        self.transforms = transforms
        self.preprocessing = preprocessing

    def __len__(self):
        return len(self.img_ids)

    def __getitem__(self, idx):
        image_name = self.img_ids[idx]
        mask = make_mask(self.df, image_name)
        image_path = os.path.join(self.data_folder, image_name)
        img = cv2.imread(image_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        augmented = self.transforms(image=img, mask=mask)
        img = augmented['image']
        mask = augmented['mask']
        if self.preprocessing:
            preprocessed = self.preprocessing(image=img, mask=mask)
            img = preprocessed['image']
            mask = preprocessed['mask']
        return img, mask


#https://github.com/okotaku/kaggle_Severstal/blob/master/severstal-src/trainer.py
def get_cutmix_data(inputs, labels, beta=1):
    def _rand_bbox(size, lam):
        W = size[2]
        H = size[3]

        cut_rat = np.sqrt(1.-lam)
        cut_w = np.int(W * cut_rat)
        cut_h = np.int(H * cut_rat)

        # uniform
        cx = np.random.randint(W)
        cy = np.random.randint(H)

        bbx1 = np.clip(cx - cut_w // 2, 0, W)
        bby1 = np.clip(cy - cut_h // 2, 0, H)
        bbx2 = np.clip(cx + cut_w // 2, 0, W)
        bby2 = np.clip(cy + cut_h // 2, 0, H)

        return bbx1, bby1, bbx2, bby2

    lam = np.random.beta(beta, beta)

    indices_shuffle = torch.randperm(inputs.size(0))
    bbx1, bby1, bbx2, bby2 = _rand_bbox(inputs.size(), lam)

    inputs[:, :, bbx1:bbx2, bby1:bby2] = inputs[indices_shuffle, :, bbx1:bbx2, bby1:bby2]
    labels[:, :, bbx1:bbx2, bby1:bby2] = labels[indices_shuffle, :, bbx1:bbx2, bby1:bby2]
    lam = 1 - ((bbx2 - bbx1) * (bby2 - bby1) / (inputs.size()[-1] * inputs.size()[-2]))

    return inputs, labels

#https://github.com/okotaku/kaggle_Severstal/blob/master/severstal-src/trainer.py
def get_mixup_data(x, y, device, alpha=1.0):
    if alpha > 0:
        lam = np.random.beta(alpha, alpha)
    else:
        lam = 1

    batch_size = x.size()[0]
    index = torch.randperm(batch_size).to(device)

    mixed_x = lam*x + (1-lam)*x[index, :]
    y_a, y_b = y, y[index]

    return mixed_x, y_a, y_b, lam


#https://github.com/okotaku/kaggle_Severstal/blob/master/severstal-src/trainer.py
def mixup_criterion(criterion, pred, y_a, y_b, lam):
    return lam * criterion(pred, y_a) + (1 - lam) * criterion(pred, y_b)
