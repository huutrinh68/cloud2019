import argparse
import os
import sys
import torch 
import torch.nn as nn
import cv2
import glob
import json
from tqdm import tqdm 
import pandas as pd 
import numpy as np
import torch.functional as F
import torch.nn.functional as F
import segmentation_models_pytorch as smp
from torch.utils.data import TensorDataset, DataLoader, Dataset
from dataset import CloudDataset, get_validation_augmentation, get_preprocessing, post_process, mask2rle
from util import set_global_seed, Logger, AverageMeter, report_checkpoint
from sklearn.model_selection import train_test_split
from model import my_model
from dataset import get_dataloader, get_validation_augmentation, get_preprocessing, get_test_ids
import gc
import copy

model_path=[
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold0/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold1/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold2/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold3/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold4/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold5/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold6/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold7/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold8/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold9/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold10/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold11/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold12/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold13/checkpoints/top1.pth',
    # 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold14/checkpoints/top1.pth',
    
    # selected
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold0/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold1/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold2/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold3/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold4/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold5/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold6/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold7/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold8/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold9/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold10/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold11/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold12/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold13/checkpoints/top1.pth',
    'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold14/checkpoints/top1.pth',

    # selected
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold0/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold1/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold2/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold3/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold4/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold5/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold6/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold7/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold8/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold9/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold10/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold11/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold12/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold13/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold14/checkpoints/top1.pth',

    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold0/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold1/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold2/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold3/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold4/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold5/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold6/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold7/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold8/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold9/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold10/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold11/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold12/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold13/checkpoints/top1.pth',
    'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold14/checkpoints/top1.pth',

    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/0/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/1/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/2/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/3/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/4/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/5/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/6/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/7/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/8/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/9/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/10/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/11/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/12/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/13/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet/checkpoint/14/top1.pth',

    # selected
    'runs/resnet152/resnet152_linknet_resnet152_fold0/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold1/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold2/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold3/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold4/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold5/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold6/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold7/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold8/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold9/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold10/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold11/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold12/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold13/checkpoints/top1.pth',
    'runs/resnet152/resnet152_linknet_resnet152_fold14/checkpoints/top1.pth',

    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/0/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/1/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/2/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/3/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/4/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/5/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/6/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/7/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/8/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/9/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/10/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/11/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/12/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/13/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/14/top1.pth',


    '/home/citynow-cloud/data/cloud2019/model006_unet_resnet152/checkpoint/0/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model006_unet_resnet152/checkpoint/1/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model006_unet_resnet152/checkpoint/2/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model006_unet_resnet152/checkpoint/3/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model006_unet_resnet152/checkpoint/4/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model006_unet_resnet152/checkpoint/5/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model006_unet_resnet152/checkpoint/6/top1.pth',

    '/home/citynow-cloud/data/cloud2019/model007_fpn_resnet152/checkpoint/0/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model007_fpn_resnet152/checkpoint/1/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model007_fpn_resnet152/checkpoint/2/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model007_fpn_resnet152/checkpoint/3/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model007_fpn_resnet152/checkpoint/4/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model007_fpn_resnet152/checkpoint/5/top1.pth',
    '/home/citynow-cloud/data/cloud2019/model007_fpn_resnet152/checkpoint/6/top1.pth',

    # '/home/citynow-cloud/data/cloud2019/model008_linknet_resnet152/checkpoint/0/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model008_linknet_resnet152/checkpoint/1/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model008_linknet_resnet152/checkpoint/2/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model008_linknet_resnet152/checkpoint/3/top1.pth',
    # '/home/citynow-cloud/data/cloud2019/model008_linknet_resnet152/checkpoint/4/top1.pth',
    ]

sigmoid = lambda x: 1 / (1 + np.exp(-x))

#test time augmentation  -----------------------
def null_augment   (input): return input
def flip_lr_augment(input): return torch.flip(input, dims=[2])
def flip_ud_augment(input): return torch.flip(input, dims=[3])

def null_inverse_augment   (logit): return logit
def flip_lr_inverse_augment(logit): return torch.flip(logit, dims=[2])
def flip_ud_inverse_augment(logit): return torch.flip(logit, dims=[3])

augment = (
    (null_augment,   null_inverse_augment   ),
    (flip_lr_augment,flip_lr_inverse_augment),
    (flip_ud_augment,flip_ud_inverse_augment),
)

def load_model(path, args, log, i):
    if 'runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold' in path:
        print(path)
        print('FPN')
        model = smp.FPN(
            encoder_name='resnet152', 
            encoder_weights='imagenet',
            classes=args.classes,
            activation=args.activation
        )
    elif 'runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold' in path:
        print(path)
        print('UNET')
        model = smp.Unet(
            encoder_name='resnet152', 
            encoder_weights='imagenet',
            classes=args.classes,
            activation=args.activation
        )
    elif 'runs/resnet152/unet_0663/resnet152_15folds_retrain_fold' in path:
        print(path)
        print('UNET')
        model = smp.Unet(
            encoder_name='resnet152', 
            encoder_weights='imagenet',
            classes=args.classes,
            activation=args.activation
        )
    elif 'model001_unet/checkpoint/' in path:
        print(path)
        print('UNET')
        model = smp.Unet(
            encoder_name='resnet152', 
            encoder_weights='imagenet',
            classes=args.classes,
            activation=args.activation
        )
    elif 'runs/resnet152/resnet152_linknet_resnet152_fold' in path:
        print(path)
        print('LINKNET')
        model = smp.Linknet(
            encoder_name='resnet152', 
            encoder_weights='imagenet',
            classes=args.classes,
            activation=args.activation
        )
    elif '/home/citynow-cloud/data/cloud2019/model001_unet_383x576/checkpoint/' in path:
        print(path)
        print('UNET')
        model = smp.Unet(
            encoder_name='resnet152', 
            encoder_weights='imagenet',
            classes=args.classes,
            activation=args.activation
        )
    elif '/home/citynow-cloud/data/cloud2019/model006_unet_resnet152/checkpoint' in path:
        print(path)
        print('UNET')
        model = smp.Unet(
            encoder_name='resnet152', 
            encoder_weights='imagenet',
            classes=args.classes,
            activation=args.activation
        )
    elif '/home/citynow-cloud/data/cloud2019/model007_fpn_resnet152/checkpoint/' in path:
        print(path)
        print('FPN')
        model = smp.FPN(
            encoder_name='resnet152', 
            encoder_weights='imagenet',
            classes=args.classes,
            activation=args.activation
        )
    elif '/home/citynow-cloud/data/cloud2019/model008_linknet_resnet152/checkpoint' in path:
        print(path)
        print('LINKNET')
        model = smp.Linknet(
            encoder_name='resnet152', 
            encoder_weights='imagenet',
            classes=args.classes,
            activation=args.activation
        )

    # model = my_model(args, log)
    try:
        # load checkpoint
        if i %3 == 0:
            device = 'cuda:1'
        elif i % 3 == 1:
            device = 'cuda:2'
        else:
            device = 'cuda:3'
        # device = args.device
        # checkpoint = torch.load(path, map_location=args.device)
        checkpoint = torch.load(path, map_location=device)
        report_checkpoint(checkpoint, log)
    except FileNotFoundError as err:
        log.write(f'\n{err}\n')
        sys.exit(1)
    if 'state_dict' in checkpoint:
        model.load_state_dict(checkpoint["state_dict"])
    elif 'model' in checkpoint:
        model.load_state_dict(checkpoint["model"])
    # device = args.device
    # model.to(device)
    if i % 3 == 0:
        model.to('cuda:1')
    elif i % 3 == 1:
        model.to('cuda:2')
    else:
        model.to('cuda:3')
    model.eval()
    log.write(f'path    = {path}\n')
    log.write(f'device  = {device}\n')
    log.write(f'model   = eval\n')

    return model

def make_submission(args):
    # setting logger
    log = Logger()

    # set directory
    directory_prefix = f'{args.model}'
    log_dir = os.path.join('runs', directory_prefix)
    os.makedirs(log_dir, exist_ok=True)

    # write setting json file
    config_fname = os.path.join(log_dir, f'test_{directory_prefix}.json')
    with open(config_fname, 'w') as f:
        test_session_args = vars(args)
        f.write(json.dumps(test_session_args, indent=2))
    
    # set up log
    log.open(os.path.join(log_dir, f'log.test_{directory_prefix}.txt'), mode='a')

    # setup device
    os.environ['CUDA_VISIBLE_DEVICES'] = '0,1,2,3'
    device = args.device
    log.write(f'device    = {args.device}\n')

    sub = pd.read_csv(f'{args.data_dir}/sample_submission.csv')
    log.write(f'len(test) = {len(sub)}\n')
    log.write(f'tta       = {args.tta}\n')
    
    # find optimized threshold and minimum size
    class_params = {}
    if args.classify_each_class:
        log.write('\nset threshold, minsize for each class...\n')
        class_params[0] =  (0.6, 20000)
        class_params[1] =  (0.6, 20000)
        class_params[2] =  (0.6, 22500)
        class_params[3] =  (0.6, 15000)

        for i in range(args.classes):
            log.write(f'class {i}: (threshold, minsize) = {class_params[i]}\n')
    else:
        log.write('\nset threshold, misize for all class onetime...\n')
        best_threshold = 0.6
        best_size = 20000
        log.write(f'allclasses: (threshold, minsize)= ({best_threshold}, {best_size})\n')
        
    if args.ensembles:
        log.write('\nensemble model...\n')
        global model_path
        models = []
        # for i, path in enumerate(args.ensembles):
        for i, path in enumerate(model_path):
            models.append(load_model(path, args, log, i))
        log.write(f'n_models= {len(models)}\n')
    else:
        log.write('\nsingle model...\n')
        model_path = 'runs/resnet152_fold0/checkpoints/top1.pth'
        # model_path = '/home/citynow/trinhnh/cloud2019/backup/resnet152_800train_651/best_model_resnet152/resnet152_fold_0_model_best_loss.pt'
        model = load_model(model_path, args, log)

    # load test data
    all_ids, test_ids = get_test_ids(args.data_dir)
    log.write(f'len(test ids)   = {len(test_ids)}\n')

    preprocess_input = smp.encoders.get_preprocessing_fn(args.encoder_name, args.encoder_weights)
    log.write('preprocess-input is done\n')

    valid_augmentation = get_validation_augmentation()
    valid_augmentation_halfdsize = get_validation_augmentation(halfsize=True)
    valid_preprocessing = get_preprocessing(preprocess_input)
    log.write('test-augmentation is done\n')
    log.write('test-preprocessing is done\n')
      
    test_loader = get_dataloader(args.data_dir, all_ids, 'test', test_ids, args.batch_size, args.num_workers, \
        valid_augmentation, valid_preprocessing, None, args)
    test_loader_halfsize = get_dataloader(args.data_dir, all_ids, 'test', test_ids, args.batch_size, args.num_workers, \
        valid_augmentation_halfdsize, valid_preprocessing, None, args)
    log.write(f'len(test_loader = {len(test_loader)})\n')
    log.write(f'len(test_loader_halfsize = {len(test_loader_halfsize)})\n')

    # release memory
    del all_ids, test_ids
    gc.collect()


    # statistic
    have_mask = 0
    no_mask = 0
    encoded_pixels = []
    torch.cuda.empty_cache()
    for i, (test_batch_origin, test_batch_halfsize) in enumerate(zip(tqdm(test_loader), test_loader_halfsize)):
        test_batch = copy.deepcopy(test_batch_origin)
        if args.ensembles:
            # outputs_total = []
            for m, model in enumerate(models):
                if m >= 60:
                    test_batch = copy.deepcopy(test_batch_halfsize)
                if m % 3 == 0:
                    device = 'cuda:1'
                elif m % 3 == 1:
                    device = 'cuda:2'
                else:
                    device = 'cuda:3'
                # tta #########################################
                if args.tta:
                    for k, (a, inv_a) in enumerate(augment):
                        logit = model(a(test_batch[0].to(device)))
                        p = inv_a(torch.sigmoid(logit))
                        if k ==0:
                            probability  = 0.8*p
                        else:
                            probability += 0.1*p
                    # probability = probability/len(augment)
                    outputs = probability
                    outputs = outputs.cpu().detach().numpy()

                    # release memory
                    del probability
                    gc.collect()
                ###############################################

                if m == 0:
                    outputs_total = outputs
                elif m <=59:
                    outputs_total += outputs
                elif m == 60:
                    outputs_total_halfsize = outputs
                elif m > 60:
                    outputs_total_halfsize += outputs

            preds = outputs_total/60 #len(models) 
            preds_halfsize = outputs_total_halfsize/14 #len(models) 

            del outputs_total, outputs_total_halfsize, outputs, model
            gc.collect()
            torch.cuda.empty_cache()
        else:
            if args.tta:
                flipped_test_batch = torch.flip(test_batch[0].to(device), dims=(3,))
                flipped_outputs = torch.sigmoid(model(flipped_test_batch))
                flipped_outputs = torch.flip(flipped_outputs, dims=(3,))

                preds = torch.sigmoid(model(test_batch[0].to(device)))
                preds += flipped_outputs
                preds *= 0.5

                del flipped_outputs, flipped_test_batch, test_batch
                gc.collect()
                torch.cuda.empty_cache()
            else:
                preds = torch.sigmoid(model(test_batch[0].to(device)))
            

        for i, (batch, batch_halfsize) in enumerate(zip(preds, preds_halfsize)):
            for j, (probability_origin, probability_halfsize) in enumerate(zip(batch, batch_halfsize)):
                # probability = probability.cpu().detach().numpy()
                if probability_origin.shape != (350, 525):
                    probability_origin = cv2.resize(probability_origin, dsize=(525, 350), interpolation=cv2.INTER_CUBIC)

                if probability_halfsize.shape != (350, 525):
                    probability_halfsize = cv2.resize(probability_halfsize, dsize=(525, 350), interpolation=cv2.INTER_CUBIC)

                probability = (60*probability_origin+14*probability_halfsize)/74.0

                if args.classify_each_class:
                    predict, num_predict = post_process(sigmoid(probability), class_params[j%4][0], class_params[j%4][1])
                else:
                    predict, num_predict = post_process(sigmoid(probability), best_threshold, best_size)
                if num_predict == 0:
                    encoded_pixels.append('')
                    no_mask += 1
                else:
                    r = mask2rle(predict)
                    encoded_pixels.append(r)
                    have_mask += 1

                del probability, probability_origin, probability_halfsize, predict
                gc.collect()
                torch.cuda.empty_cache()
            del batch
            gc.collect() 
            torch.cuda.empty_cache()
        del test_batch
        gc.collect()
        torch.cuda.empty_cache()
    sub['EncodedPixels'] = encoded_pixels
    sub.to_csv(args.encoder_name+'_45folds_submission.csv', columns=['Image_Label', 'EncodedPixels'], index=False)

    ## log
    log.write('\n------------------------------\n')
    log.write(f'mask size       = ({525}, {350})\n')
    log.write(f'len(sub)        = {len(sub)}\n')
    log.write(f'have-mask       = {have_mask}\n')
    log.write(f'no-mask         = {no_mask}\n')
    log.write(f'\nheader of submit file\n')
    log.write(f'{sub.head()}\n')
    log.write('success!\n')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='understanding_cloud_organization: predicting test image to submit')
    parser.add_argument('-dd', '--data_dir', type=str, default='../../input/understanding_cloud_organization', help='Data directory')
    parser.add_argument("--task", help="classification or segmentation", type=str, default='segmentation')
    parser.add_argument('-m', '--model', type=str, default='resnet152', help='')
    parser.add_argument('-en', '--encoder_name', default='resnet152', type=str, help='Encoder for model')
    parser.add_argument('-ew', '--encoder_weights', default='imagenet', type=str, help='Pretrained weight for model')
    parser.add_argument('-bs', '--batch_size', default=1, type=int, help='Batchsize number')
    parser.add_argument('-nw', '--num_workers', default=4, type=int, help='Worker number')
    parser.add_argument('--classes', default=4, type=int, help='Number of class')
    parser.add_argument('--activation', default='sigmoid', type=str, help='Activation type')
    parser.add_argument('--classify_each_class', default=1, type=int, help='Using threshold for each class')
    parser.add_argument('-d', '--device', default='cpu', type=str, help='Run on GPU or CPU')
    parser.add_argument('--ensembles', action='append', type=str, help='model path to ensemble', default=None)
    parser.add_argument('--tta', default=1, type=int, help='Predict use TTA')
    parser.add_argument('--interpolation', default='cv2.INTER_CUBIC', type=str, help='cv2.INTER_CUBIC or cv2.INTER_LINEAR')
    parser.add_argument('--convex', default=1, type=int, help='use convex process for post-processing')
    
    args = parser.parse_args()

    make_submission(args)
    # cal_probabilities()
    # load_models()
    # snapshot_ensemble()
