
# -*- coding: utf-8 -*-
import torch.nn as nn
import torch
from torch.nn import functional as F
from torchvision import models
import torchvision
import pretrainedmodels
import segmentation_models_pytorch as smp

def my_model(args, log):
    log.write('\n------------------------------\n')
    encoder_name = args.encoder_name
    encoder_weights = args.encoder_weights
    activation = args.activation

    log.write(f'loaded model arch = {args.encoder_name}\n')
    log.write(f'weight            = {args.encoder_weights}\n')
    log.write(f'classes           = {args.classes}\n')
    log.write(f'activation        = {args.activation}\n')
    log.write(f'task              = {args.task}\n')

    if args.task == 'segmentation':
        # model = smp.Unet(encoder_name=encoder_name, 
        #                 encoder_weights=encoder_weights,
        #                 classes=args.classes,
        #                 activation=activation)
        model = smp.Linknet(encoder_name=encoder_name, 
                        encoder_weights=encoder_weights,
                        classes=args.classes,
                        activation=activation)
        # model = smp.FPN(encoder_name=encoder_name, 
        #                 encoder_weights=encoder_weights,
        #                 classes=args.classes,
        #                 activation=activation)

    elif args.task == 'classification':
        log.write(f'source            = {args.source}\n')
        log.write(f'head              = {args.head}\n')

        if args.source == 'pretrainedmodels':
            model_fn = pretrainedmodels.__dict__[encoder_name]
            model = model_fn(num_classes=1000, pretrained=encoder_weights)
        elif args.source == 'torchvision':
            model = torchvision.models.__dict__[encoder](pretrained=encoder_weights)
        
        if args.head == 'simple':
            model.last_linear = nn.Linear(model.last_linear.in_features, args.classes)
        else:
            model = Net(net=model)

    model = model.to(args.device)
    log.write(f'moved model to    = {args.device}')

    return model


class Flatten(nn.Module):
    """
    Simple class for flattening layer.
    """
    def forward(self, x):
        return x.view(x.size()[0], -1)


class Net(nn.Module):
    def __init__(
            self,
            num_classes: int = 4,
            p: float = 0.2,
            net = None) -> None:
        """
        Custom head architecture
        Args:
            num_classes: number of classes
            p: dropout probability
            net: original model
        """
        super().__init__()
        modules = list(net.children())[:-1]
        n_feats = list(net.children())[-1].in_features
        
        # add custom head
        modules += [nn.Sequential(
            Flatten(),
            nn.BatchNorm1d(2048),
            nn.Dropout(p),
            nn.Linear(2048, n_feats),
            nn.Linear(n_feats, num_classes),
            nn.Sigmoid()
        )]
        self.net = nn.Sequential(*modules)

    def forward(self, x):
        logits = self.net(x)
        return logits

