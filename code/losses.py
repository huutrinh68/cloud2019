import torch.nn as nn
import segmentation_models_pytorch as smp

def get_criterion(args, log):
    log.write('\n------------------------------\n')
    if args.task == 'segmentation':
        if args.losses.lower() == 'bcediceloss':
            criterion = smp.utils.losses.BCEDiceLoss(eps=1.)
            log.write(f'criterion = bcediceloss\n')

    elif args.task == 'classification':
        criterion = nn.BCEWithLogitsLoss()
        log.write(f'criterion = bcewithlogitloss\n')

    return criterion