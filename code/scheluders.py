from torch.optim.lr_scheduler import ReduceLROnPlateau

def get_scheduler(optimizer, type, args, log):
    if type.lower() == 'reducelronplateau':
        scheduler = ReduceLROnPlateau(optimizer, factor=args.factor, patience=args.patience)
    
    log.write('\n------------------------------\n')
    log.write(f'scheduler = {type.lower()}\n')
    log.write(f'factor    = {args.factor}\n')
    log.write(f'patience  = {args.patience}\n')

    return scheduler