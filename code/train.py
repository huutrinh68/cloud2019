from __future__ import absolute_import

import argparse
import os
import sys
import cv2 
import time
import json
import gc
import copy
import numpy as np 
# from apex import amp

from datetime import datetime
import torch
import torch.nn as nn
import torch.functional as F
import torch.nn.functional as F

from sklearn.metrics import f1_score, roc_auc_score

import segmentation_models_pytorch as smp
from model import my_model
from optimizers import get_optimizer
from scheluders import get_scheduler
from losses import get_criterion
from metrics import dice_score, mean_dice_coef
from dataset import get_train_valid_ids, get_dataloader, \
    get_training_augmentation, get_validation_augmentation, get_preprocessing

from utils import set_global_seed, Logger, AverageMeter, \
    save_top_epochs, save_model, report_checkpoint, accumulate

# pylog = logging.getLogger(__name__)
import warnings
warnings.filterwarnings('ignore')


########## train ##########
def train(model, ema_model, train_loader, optimizer, criterion, epoch, lr, args):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    dices = AverageMeter()

    # switch to train mode
    model.train() 
    
    num_its = len(train_loader)
    end = time.time()
    
    for idx, (inputs, labels) in enumerate(train_loader, 0):
        # measure data loading time
        data_time.update(time.time() - end)

        # zero out gradients so we can accumulate new ones over batches
        optimizer.zero_grad()

        # move data to device
        inputs = inputs.to(args.device, dtype=torch.float)
        labels = labels.to(args.device, dtype=torch.float)
        
        with torch.set_grad_enabled(True):
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            losses.update(loss.item()) # or losses.update(loss.item(), inputs.size(0))

            loss = loss / args.accumulate_step
            loss.backward()
            # with amp.scale_loss(loss, optimizer) as scaled_loss:
            #     scaled_loss.backward()

            torch.nn.utils.clip_grad_norm(model.parameters(), 1.0)
            optimizer.step()

        if args.task == 'segmentation':
            # probs = torch.sigmoid(outputs)
            # dice = dice_score(probs, labels)
            # dice = dice_score(outputs, labels)
            dice = mean_dice_coef(outputs, labels)
        elif args.task == 'classification':
            labels = labels.cpu().detach().numpy()
            outputs = outputs.sigmoid().cpu().detach().numpy() > args.threshold # bigger 0.75 may be have mask
            dice = f1_score(labels, outputs, average='macro')
        
        dices.update(dice.item()) # or dices.update(dice.item(), inputs.size(0))

        if args.ema:
            if epoch >= args.ema_start:
                accumulate(ema_model, model, decay=args.ema_decay)
            else:
                accumulate(ema_model, model, decay=0)

        # measure elapsed time
        batch_time.update(time.time() -end)
        end = time.time()

        # update params by accumulate gradient
        if idx ==0 or (idx+1) % args.accumulate_step == 0 or (idx+1) == num_its:
            optimizer.step() 
            optimizer.zero_grad()
            
            print('\r', end='', flush=True)
            print('\r%5.1f   %5d    %0.6f   |  %0.4f  %0.4f  |  ... ' % \
                  (epoch - 1 + (idx + 1) / num_its, idx + 1, lr, losses.avg, dices.avg), \
                   end='', flush=True)

    return idx, losses.avg, dices.avg


########## valid ##########
def valid(model, valid_loader, criterion, args):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    dices = AverageMeter()

    # switch to evaluate mode
    model.eval()

    end = time.time()

    for it, (inputs, labels) in enumerate(valid_loader, 0):
        # measure data loading time
        data_time.update(time.time() -end)

        # move variable to device
        inputs = inputs.to(args.device, dtype=torch.float)
        labels = labels.to(args.device, dtype=torch.float)

        outputs = model(inputs)
        loss = criterion(outputs, labels)
        losses.update(loss.item()) # or losses.update(loss.item(), inputs.size(0))

        if args.task == 'segmentation':
            # probs = torch.sigmoid(outputs)
            # dice = dice_score(probs, labels)
            # dice = dice_score(outputs, labels)
            dice = mean_dice_coef(outputs, labels)
        elif args.task == 'classification':
            labels = labels.cpu().detach().numpy()
            outputs = outputs.sigmoid().cpu().detach().numpy() > args.threshold # bigger 0.75 may be have mask
            dice = f1_score(labels, outputs, average='macro')
            
        dices.update(dice.item())

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

    return losses.avg, dices.avg


########## main ##########
def main(args):
    # setting device
    os.environ["CUDA_VISIBLE_DEVICES"] = '0,1,2,3'
    torch.backends.cudnn.benchmark=True

    # setting logger
    log = Logger()

    folds = args.folds
    
    if folds is None or len(folds)== 0:
        folds = [None]

    for fold in folds:
        torch.cuda.empty_cache()
        gc.collect()

        # set directory
        directory_prefix = f'{args.model}_15folds_unet_contrast_test_dice'
        if fold is not None:
            directory_prefix += f'_fold{fold}'
            log_dir = os.path.join('runs', f'{args.model}', directory_prefix)    
        else:
            log_dir = os.path.join('runs', directory_prefix)
        
        os.makedirs(log_dir, exist_ok=True)
        model_out_dir = os.path.join(log_dir, 'checkpoints')
        os.makedirs(model_out_dir, exist_ok=True)
        
        # write setting json file
        config_fname = os.path.join(log_dir, f'train_{args.model}_{directory_prefix}.json')
        with open(config_fname, 'w') as f:
            train_session_args = vars(args)
            f.write(json.dumps(train_session_args, indent=2))
        
        # set up log
        log.open(os.path.join(log_dir, f'log.{args.model}.train_fold{fold}.txt'), mode='a')

        start_epoch = 0
        best_epoch = 0
        best_dice = 0
        best_dice_arr = np.zeros(3)

        set_global_seed(args.seed)
        log.write(f'\nsetting seed = {args.seed}\n')

        # load csv and split train/valid
        if args.task == 'segmentation':
            one_hot_labels = None
            all_ids, train_ids, valid_ids = get_train_valid_ids(fold, args, log)
        elif args.task == 'classification':
            all_ids, train_ids, valid_ids, one_hot_labels = get_train_valid_ids(fold, args, log)
        log.write('load ids is done\n')

        preprocess_input = smp.encoders.get_preprocessing_fn(args.encoder_name, args.encoder_weights)

        # load train data
        train_augmentation = get_training_augmentation()
        log.write('train-augmentation is done\n')
        train_preprocessing = get_preprocessing(preprocess_input)
        log.write('train-preprocessing is done\n')
        train_loader = get_dataloader(
            args.data_dir, 
            all_ids, 
            'train', 
            train_ids, 
            args.batch_size, 
            args.num_workers,
            train_augmentation, 
            train_preprocessing,
            one_hot_labels,
            args)
        log.write(f'len(train loader) = {len(train_loader)}\n')

        # load valid data
        valid_augmentation = get_validation_augmentation()
        log.write('valid-augmentation is done\n')
        valid_preprocessing = get_preprocessing(preprocess_input)
        log.write('valid-preprocessing is done\n')
        valid_loader = get_dataloader(
            args.data_dir, 
            all_ids, 
            'valid', 
            valid_ids,
            args.batch_size, 
            args.num_workers,
            valid_augmentation, 
            valid_preprocessing,
            one_hot_labels,
            args)
        log.write(f'len(valid loader) = {len(valid_loader)}\n')

        # model ########################
        model = my_model(args, log)

        if args.ema:
            ema_model = copy.deepcopy(model)
            ema_model.to(args.device)
        else:
            ema_model = None

        # optimizer ####################
        optimizer = get_optimizer(model, args, log)
        
        # use amp to mixed precision fp16
        # model, optimizer = amp.initialize(model, optimizer, opt_level="O1")

        # scheduler ####################
        scheduler = get_scheduler(optimizer, 'reducelronplateau', args, log)

        # criterion ####################
        criterion = get_criterion(args, log)

        # tensorboard logs
        # TODO: add tensorboard log

        if args.resume:
            model_fpath = os.path.join(model_out_dir, args.resume)
            if os.path.isfile(model_fpath):
                # load checkpoint weights and update model and optimizer
                log.write(f">> Loading checkpoint:\n>> '{model_fpath}'\n")
                
                checkpoint = torch.load(model_fpath)
                report_checkpoint(checkpoint, log)

                start_epoch = checkpoint['epoch']
                best_epoch = checkpoint['best_epoch']
                best_dice_arr = checkpoint['best_dice_arr']
                best_dice = np.max(best_dice_arr)
                if type(model) == nn.DataParallel: 
                    model.module.load_state_dict(checkpoint['state_dict'])
                else:
                    model.load_state_dict(checkpoint['state_dict'])

                optimizer_fpath = model_fpath.replace('.pth', '_optim.pth')
                if os.path.exists(optimizer_fpath):
                    log.write(f">> Loading checkpoit:\n>> '{optimizer_fpath}'\n")
                    optimizer.load_state_dict(torch.load(optimizer_fpath)['optimizer'])
                
                if args.ema:
                    ema_model_fpath =model_fpath.replace('.pth', '_ema.pth')
                    if os.path.join(ema_model_fpath):
                        log.write(f">> Loading checkpoint:\n>>{ema_model_fpath}")
                        ema_model.load_state_dict(torch.load(ema_model_fpath)['state_dict'])

                log.write(f">> Loaded checkpoint:\n>> '{model_fpath}' (epoch {checkpoint['epoch']})\n")

                del checkpoint
                gc.collect()
            else:
                log.write(f">> No checkpoint found at '{model_fpath}'\n")
        
        start_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        log.write(f'** start training here! fold{fold}th {start_time} **\n')
        log.write('\n')
        log.write('epoch    iter      rate     | smooth_loss/dice | valid_loss/dice | best_epoch/best_score |  min \n')
        log.write('----------------------------------------------------------------------------------------------- \n')
        start_epoch += 1
        for epoch in range(start_epoch, args.epochs+1):
            end = time.time()
            # set random seed for each epoch
            set_global_seed(epoch)

            # lr for encoder
            lr = optimizer.param_groups[0]['lr'] # scheduler have not get_lr
            
            idx, train_loss, train_dice = train(model, ema_model, train_loader, optimizer, criterion, epoch, lr, args)
            with torch.no_grad():
                if args.ema:
                    valid_loss, valid_dice = valid(ema_model, valid_loader, criterion, args)
                else:
                    valid_loss, valid_dice = valid(model, valid_loader, criterion, args)
            
            # decay lr if valid_loss doesn't decrease
            scheduler.step(valid_loss)

            # remember best loss and save checkpoint
            is_best = valid_dice >= best_dice
            if is_best:
                best_epoch = epoch
                best_dice = valid_dice
            
            if args.ema:
                save_top_epochs(model_out_dir, ema_model, best_dice_arr, valid_dice,
                                best_epoch, epoch, best_dice, ema=True)

            best_dice_arr = save_top_epochs(model_out_dir, model, best_dice_arr, valid_dice, 
                                            best_epoch, epoch, best_dice, ema=False)

            print('\r', end='', flush=True)
            log.write('%5.1f   %5d    %0.6f   |  %0.4f  %0.4f  |  %0.4f  %6.4f |  %6.1f     %6.4f    | %3.1f min \n' % \
                (epoch, idx + 1, lr, train_loss, train_dice, valid_loss, valid_dice, best_epoch, best_dice, (time.time() - end) / 60))

            # savemodel
            model_savename = f'{epoch:03d}'
            if args.ema:
                save_model(ema_model, model_out_dir, epoch, model_savename, best_dice_arr, is_best=is_best,
                           optimizer=optimizer, best_epoch=best_epoch, best_dice=best_dice, ema=True)
            # save every epoch
            # save_model(model, model_out_dir, epoch, model_savename, best_dice_arr, is_best=is_best, 
            #            optimizer=optimizer, best_epoch=best_epoch, best_dice=best_dice, ema=False)      
            
            if args.early_stopping:
                if epoch - best_epoch > args.early_stopping:
                    print('='*30, '>early stopped!')
                    break

            time.sleep(0.01)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Understanding cloud competition on kaggle')
    parser.add_argument('-dd', '--data_dir', type=str, default='../../input/understanding_cloud_organization', \
                        help='Data directory')
    parser.add_argument("--task", help="classification or segmentation", type=str, default='segmentation')
    parser.add_argument("--threshold", help="use when task is classification", type=float, default=0.75)
    parser.add_argument("--source", help="pretrainedmodels or torchvision", type=str, default='pretrainedmodels')
    parser.add_argument("--head", help="simple or customize for head of network", type=str, default=None)
    parser.add_argument('--seed', type=int, default=42, help='Random seed') 
    parser.add_argument('-f', '--folds', action='append', type=int, default=None)
    parser.add_argument('-m', '--model', type=str, default='resnet152', help='')
    parser.add_argument('-en', '--encoder_name', default='resnet152', type=str, \
                        help='Encoder for model')
    parser.add_argument('-ew', '--encoder_weights', default='imagenet', type=str, \
                        help='Pretrained weight for model')
    parser.add_argument('--classes', default=4, type=int, help='Number of class')
    parser.add_argument('--activation', default='sigmoid', type=str, help='Activation type')
    parser.add_argument('--losses', default='bcediceloss', type=str, help='Loss function type')
    parser.add_argument('--encoder_lr', default=5e-4, type=float, help='lr for encoder')
    parser.add_argument('--decoder_lr', default=5e-3, type=float, help='lr for decoder')
    parser.add_argument('--factor', default=0.75, type=float, help='factor for derease lr')
    parser.add_argument('--patience', default=2, type=float, help='parience for derease lr')
    parser.add_argument('-d', '--device', default='cuda:0', type=str, help='Run on GPU or CPU')
    parser.add_argument('-bs', '--batch_size', default=8, type=int, help='Batchsize number')
    parser.add_argument('-nw', '--num_workers', default=16, type=int, help='Worker number')
    parser.add_argument('-o', '--optimizer', default='Adam', help='Name of the optimizer')
    parser.add_argument('-s', '--scheduler', default='multistep', type=str, help='')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('--clipnorm', default=1.0, type=int, help='clip grad norm')
    parser.add_argument('--accumulate_step', default=10, type=int, help='Step to accumulate gradient')
    parser.add_argument('-e', '--epochs', default=200, type=int, help='Epoch to run')
    parser.add_argument('-es', '--early_stopping', type=int, default=10,
                        help='Maximum number of epochs without improvement')
    parser.add_argument('--resume', default=None, type=str, 
                        help='name of the latest checkpoint (default: None)')
    parser.add_argument('--ema', action='store_true', default=False)
    parser.add_argument('--ema_decay', type=float, default=0.9999)
    parser.add_argument('--ema_start', type=int, default=0)
    parser.add_argument('--debug', type=int, default=0)
    parser.add_argument('--augment', type=str, default='add_OpticalDistortion')

    
    args = parser.parse_args()
    main(args)
