import numpy as np 
import torch

def dice_score(predict, target, smooth=1e-5):
    # first convert to sigmoid
    predict = torch.sigmoid(predict)
    smooth = 1.

    pflat = predict.view(-1)
    tflat = target.view(-1)
    pflat = pflat.cpu().detach()
    tflat = tflat.cpu().detach()
    intersection = (pflat * tflat).sum()
    dice_score = (2. * intersection + smooth) / (pflat.sum() + tflat.sum() + smooth)

    return dice_score

#------------------------------------------------
#https://www.kaggle.com/c/understanding_cloud_organization/discussion/114093
#https://www.kaggle.com/wh1tezzz/correct-dice-metrics-for-this-competition
def single_dice_coef(y_true, y_pred_bin):
    # shape of y_true and y_pred_bin: (height, width)
    
    # numpy version -----------------------------
    # y_true = np.asarray(y_true.cpu().detach().numpy()).astype(np.bool)
    # y_pred_bin = np.asarray(y_pred_bin.cpu().detach().numpy()).astype(np.bool)

    # intersection = np.sum(y_true * y_pred_bin)
    # if (np.sum(y_true)==0) and (np.sum(y_pred_bin)==0):
    #     return 1
    # return (2*intersection+esp) / (np.sum(y_true) + np.sum(y_pred_bin)+esp)

    # torch version -----------------------------
    esp         = 1e-9
    threshold   = 0.5
    y_true      = (y_true.view(-1) > 0.5).float()
    y_pred_bin  = (y_pred_bin.view(-1) > threshold).float()

    intersection = (y_true * y_pred_bin).sum()
    if (y_true.sum()==0) and (y_pred_bin.sum()==0):
        return 1
    return (2*intersection+esp) / (y_true.sum() + y_pred_bin.sum()+esp)

def mean_dice_coef(y_true, y_pred_bin):
    # shape of y_true and y_pred_bin: (n_samples, height, width, n_channels)
    batch_size = y_true.shape[0]
    channel_num = y_true.shape[1]
    mean_dice_channel = 0.
    
    # torch version------------------------------
    with torch.no_grad(): 
        for i in range(batch_size):
            for j in range(channel_num):
                channel_dice = single_dice_coef(y_true[i, j, :, :], y_pred_bin[i, j, :, :])
                mean_dice_channel += channel_dice/(channel_num*batch_size)
    
    # numpy version -----------------------------
    # for i in range(batch_size):
    #     for j in range(channel_num):
    #         channel_dice = single_dice_coef(y_true[i, j, :, :], y_pred_bin[i, j, :, :])
    #         mean_dice_channel += channel_dice/(channel_num*batch_size)

    return mean_dice_channel
