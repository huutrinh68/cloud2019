#!/usr/bin/env bash

python ../train001.py \
--model resnet152 \
--encoder_name resnet152 \
--optimizer adam \
--device cuda:2 \
--batch_size 22 \
--num_workers 10 \
--folds 0 --folds 1 --folds 2 --folds 3 --folds 4 \
--folds 5 --folds 6 --folds 7 --folds 8 --folds 9 \
--folds 10 --folds 11 --folds 12 --folds 13 --folds 14 \
--task segmentation
