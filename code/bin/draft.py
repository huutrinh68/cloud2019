# import numpy as np
# import torch 
# np.random.seed(0)
# true = np.random.rand(10, 5, 5, 4)>0.5
# pred = np.random.rand(10, 5, 5, 4)>0.5

# def single_dice_coef(y_true, y_pred_bin):
#     # shape of y_true and y_pred_bin: (height, width)
#     intersection = np.sum(y_true * y_pred_bin)
#     if (np.sum(y_true)==0) and (np.sum(y_pred_bin)==0):
#         return 1
#     return (2*intersection) / (np.sum(y_true) + np.sum(y_pred_bin))

# def mean_dice_coef(y_true, y_pred_bin):
#     # shape of y_true and y_pred_bin: (n_samples, height, width, n_channels)
#     batch_size = y_true.shape[0]
#     channel_num = y_true.shape[-1]
#     mean_dice_channel = 0.
#     for i in range(batch_size):
#         for j in range(channel_num):
#             channel_dice = single_dice_coef(y_true[i, :, :, j], y_pred_bin[i, :, :, j])
#             mean_dice_channel += channel_dice/(channel_num*batch_size)
#     return mean_dice_channel

# def dice_coef2(y_true, y_pred):
#     y_true_f = y_true.flatten()
#     y_pred_f = y_pred.flatten()
#     union = np.sum(y_true_f) + np.sum(y_pred_f)
#     if union==0: return 1
#     intersection = np.sum(y_true_f * y_pred_f)
#     return 2. * intersection / union

# print(mean_dice_coef(true, pred))
# print(dice_coef2(true, pred))

# # 0.4884357140842496
# # 0.499001996007984

# def dice_score(predict, target, smooth=1e-5):
#     # first convert to sigmoid
#     predict = torch.sigmoid(predict)
#     smooth = 1.0

#     pflat = predict.view(-1)
#     tflat = target.view(-1)
#     pflat = pflat.cpu().detach()
#     tflat = tflat.cpu().detach()
#     intersection = (pflat * tflat).sum()
#     dice_score = (2. * intersection + smooth) / (pflat.sum() + tflat.sum() + smooth)

#     return dice_score

# true = np.random.rand(10, 5, 5, 4)
# pred = np.random.rand(10, 5, 5, 4)
# print(dice_score(torch.from_numpy(true), torch.from_numpy(pred)).item())

# def khavo(img1, img2):
#     img1 = np.asarray(img1).astype(np.bool)
#     img2 = np.asarray(img2).astype(np.bool)
#     if img1.sum() + img2.sum() == 0: return 1
#     intersection = np.logical_and(img1, img2)
#     return 2. * intersection.sum() / (img1.sum() + img2.sum())

# print(khavo(true, pred))

# true = np.asarray(true)
# print(true)

import os
import pandas as pd

data_dir = 'input/understanding_cloud_organization'
# get test ids
def get_test_ids(data_dir):
    cloud2019_test = pd.read_csv(os.path.join(data_dir, 'sample_submission.csv'))
    cloud2019_test['label'] = cloud2019_test['Image_Label'].apply(lambda x: x.split('_')[1])
    cloud2019_test['im_id'] = cloud2019_test['Image_Label'].apply(lambda x: x.split('_')[0])
    test_ids = cloud2019_test['Image_Label'].apply(lambda x: x.split('_')[0]).drop_duplicates().values

    return cloud2019_test, test_ids

cloud2019_test, test_ids = get_test_ids(data_dir)
print(len(cloud2019_test))
print(test_ids[:10])
print(len(test_ids))