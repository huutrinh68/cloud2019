#!/usr/bin/env bash

python ../train.py \
--model densenet201 \
--encoder_name densenet201 \
--device cuda:0 \
--batch_size 18 --num_workers 10 \
--folds 0 --folds 1 --folds 2 --folds 3 --folds 4 \
--folds 5 --folds 6 --folds 7 --folds 8 --folds 9 \
--task segmentation