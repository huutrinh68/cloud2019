#!/usr/bin/env bash
#https://stackoverflow.com/questions/9522631/how-to-put-a-line-comment-for-a-multi-line-command
current_date=`date +"%Y-%m-%d %T"`
echo "[***Inferencing***] "$current_date

python ../test.py \
--model resnext101_32x8d \
--encoder_name resnext101_32x8d \
--device cuda:1 \
--batch_size 4 \
--num_workers 2 \
--classify_each_class 1 \
--tta 1 \
--ensembles runs/resnext101_32x8d/resnext101_32x8d_15folds_fold0/checkpoints/top1.pth \
--ensembles runs/resnext101_32x8d/resnext101_32x8d_15folds_fold1/checkpoints/top1.pth \
--ensembles runs/resnext101_32x8d/resnext101_32x8d_15folds_fold2/checkpoints/top1.pth \
--ensembles runs/resnext101_32x8d/resnext101_32x8d_15folds_fold3/checkpoints/top1.pth \
--ensembles runs/resnext101_32x8d/resnext101_32x8d_15folds_fold4/checkpoints/top1.pth \
--ensembles runs/resnext101_32x8d/resnext101_32x8d_15folds_fold5/checkpoints/top1.pth \
--ensembles runs/resnext101_32x8d/resnext101_32x8d_15folds_fold6/checkpoints/top1.pth \
--ensembles runs/resnext101_32x8d/resnext101_32x8d_15folds_fold7/checkpoints/top1.pth

# ./submit.sh