#!/usr/bin/env bash
#https://stackoverflow.com/questions/9522631/how-to-put-a-line-comment-for-a-multi-line-command
current_date=`date +"%Y-%m-%d %T"`
echo "[***Inferencing***] "$current_date

python ../test.py \
--model efficientnet-b5 \
--encoder_name efficientnet-b5 \
--device cuda:1 \
--batch_size 1 \
--num_workers 8 \
--classify_each_class 1 \
--tta 0 \
--ensembles runs/efficientnet-b5/efficientnet-b5_fold0/checkpoints/top1.pth \
--ensembles runs/efficientnet-b5/efficientnet-b5_fold0/checkpoints/top2.pth \
--ensembles runs/efficientnet-b5/efficientnet-b5_fold0/checkpoints/top3.pth
