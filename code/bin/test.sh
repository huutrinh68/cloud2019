#!/usr/bin/env bash
#https://stackoverflow.com/questions/9522631/how-to-put-a-line-comment-for-a-multi-line-command
current_date=`date +"%Y-%m-%d %T"`
echo "[***Inferencing***] "$current_date

python ../test_new.py \
--model resnet152 \
--encoder_name resnet152 \
--device cuda \
--batch_size 8 \
--num_workers 30 \
--classify_each_class 1 \
--tta 1 \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold0/checkpoints/top1.pth \
$(:||:--ensembles runs/resnet152/resnet152_15folds_retrain_fold0/checkpoints/top2.pth \  )\
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold1/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold2/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold3/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold4/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold5/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold6/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold7/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold8/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold9/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold10/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold11/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold12/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold13/checkpoints/top1.pth \
--ensembles runs/resnet152/0663/resnet152_15folds_retrain_fold14/checkpoints/top1.pth

# ./submit.sh

