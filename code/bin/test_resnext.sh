#!/usr/bin/env bash
#https://stackoverflow.com/questions/9522631/how-to-put-a-line-comment-for-a-multi-line-command
current_date=`date +"%Y-%m-%d %T"`
echo "[***Inferencing***] "$current_date

python ../test.py \
--model resnext50_32x4d \
--encoder_name resnext50_32x4d \
--device cuda:0 \
--batch_size 8 \
--num_workers 2 \
--classify_each_class 1 \
--tta 1 \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold0/checkpoints/top1.pth \
$(:||:--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold0/checkpoints/top2.pth \  )\
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold1/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold2/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold3/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold4/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold5/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold6/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold7/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold8/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold9/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold10/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold11/checkpoints/top1.pth \
--ensembles runs/resnext50_32x4d/resnext50_32x4d_15folds_fold12/checkpoints/top1.pth \

kaggle competitions submit -c understanding_cloud_organization -f resnext50_32x4d_submission.csv \
-m "predict 15folds (verival, horizontal)tta"

