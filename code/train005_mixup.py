from __future__ import absolute_import
from code.utils.logger import logger, log
logger.setup('./logs', name='log_model005_mixup')

import argparse
import os
import sys
import time
from datetime import datetime

import numpy as np 
import torch
import gc

from code.utils.config import Config
import code.utils.util as util
from code.datasets.dataset import get_train_valid_ids, get_training_augmentation, \
    get_preprocessing, get_dataloader, get_validation_augmentation, get_cutmix_data, get_mixup_data, mixup_criterion
import code.factory as factory
from code.metrics import dice_score, mean_dice_coef
import segmentation_models_pytorch as smp

import warnings
warnings.filterwarnings('ignore')

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES']='0,1,2,3'
device = torch.device(f'cuda:0' if torch.cuda.is_available() else 'cpu')



# get args from command line --------------------
def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument('config')
    parser.add_argument('--folds', action='append', type=int, default=None)
    parser.add_argument('--gpu', nargs='+', type=int)
    parser.add_argument('--snapshot')
    parser.add_argument('--output') 
    parser.add_argument('--n_tta', default=1, type=int)
    
    return parser.parse_args()

# ########## train ##########
def do_train(model, train_loader, optimizer, criterion, epoch, cfg):
    losses = util.AverageMeter()
    scores = util.AverageMeter()

    # switch to train mode
    model.train() 
    
    num_its = len(train_loader)
    end = time.time()

    train_result = {}
    
    for idx, (inputs, labels) in enumerate(train_loader, 0):
        # zero out gradients so we can accumulate new ones over batches
        optimizer.zero_grad()

        # # cutmix
        # if cfg.cutmix_prob is not None and np.random.rand() < cfg.cutmix_prob:
        #     inputs, labels = get_cutmix_data(
        #         inputs,
        #         labels,
        #         beta=1.0
        #     )

        # mixup
        if cfg.mixup_alpha is not None:
            inputs, labels_a, labels_b, lam = get_mixup_data(inputs, labels, cfg.device, alpha=mixup_alpha)
            # move data to device
            inputs = inputs.to(cfg.device, type=torch.float) 
            labels_a = labels_a.to(cfg.device, type=torch.float) 
            labels_b = labels_b.to(cfg.device, type=torch.float)
        else:
            # move data to device
            inputs = inputs.to(cfg.device, dtype=torch.float)
            labels = labels.to(cfg.device, dtype=torch.float)
        
        with torch.set_grad_enabled(True):
            outputs = model(inputs)

            # mixup
            if cfg.mixup_alpha is not None:
                loss = mixup_criterion(criterion, output, labels_a, labels_b, lam)
            else:
                loss = criterion(outputs, labels)

            losses.update(loss.item())

            loss = loss / cfg.accumulate_step
            loss.backward()

            torch.nn.utils.clip_grad_norm(model.parameters(), 1.0)
            optimizer.step()

        score = dice_score(outputs, labels)
        scores.update(score.item())

        # measure elapsed time
        end = time.time()

        # update params by accumulate gradient
        if idx ==0 or (idx+1) % cfg.accumulate_step == 0 or (idx+1) == num_its:
            optimizer.step() 
            optimizer.zero_grad()
        
    train_result['loss'] = losses.avg
    train_result['score'] = scores.avg

    return train_result

    
########## valid ##########
def do_valid(model, valid_loader, criterion, cfg):
    losses = util.AverageMeter()
    scores = util.AverageMeter()

    # switch to evaluate mode
    model.eval()

    valid_result = {}
    for it, (inputs, labels) in enumerate(valid_loader, 0):
        # move variable to device
        inputs = inputs.to(cfg.device, dtype=torch.float)
        labels = labels.to(cfg.device, dtype=torch.float)

        outputs = model(inputs)
        loss = criterion(outputs, labels)
        losses.update(loss.item())

        score = dice_score(outputs, labels)
        scores.update(score.item())

    valid_result['loss'] = losses.avg 
    valid_result['score'] = scores.avg

    return valid_result


########## main ##########
def main():
    args = get_args()
    cfg  = Config.fromfile(args.config)

    # copy command line args to cfg
    cfg.folds = args.folds
    cfg.snapshot = args.snapshot
    cfg.output = args.output
    cfg.n_tta = args.n_tta
    cfg.gpu = args.gpu
    cfg.device = device
    
    if cfg.folds is None or len(cfg.folds)== 0:
        cfg.folds = [None]

    for fold in cfg.folds:
        torch.cuda.empty_cache()
        gc.collect()

        # set directory
        for f in ['checkpoint', 'train', 'valid', 'test', 'backup']: os.makedirs(cfg.workdir+'/'+f, exist_ok=True)
        start_epoch = 0
        best_epoch = 0
        best_dice = 0
        best_dice_arr = np.zeros(3)

        util.set_global_seed(cfg.seed)
        log.info(f'setting seed = {cfg.seed}')

        # load csv and split train/valid
        all_ids, train_ids, valid_ids = get_train_valid_ids(fold, cfg)
        preprocess_input = smp.encoders.get_preprocessing_fn(cfg.model.params.encoder_name, cfg.model.params.encoder_weights)

        # load train data
        log.info('** train dataset setting **')
        train_augmentation = get_training_augmentation()
        log.info(f'train-augmentation: {train_augmentation}')
        train_preprocessing = get_preprocessing(preprocess_input)
        
        train_loader = get_dataloader(
            cfg.data_dir, 
            all_ids, 
            'train', 
            train_ids, 
            cfg.batch_size, 
            cfg.num_workers,
            train_augmentation, 
            train_preprocessing)
        log.info(f'len(train loader) = {len(train_loader)}\n')

        # load valid data
        log.info('** valid dataset setting **')
        valid_augmentation = get_validation_augmentation()
        log.info(f'valid-augmentation: {valid_augmentation}')
        valid_preprocessing = get_preprocessing(preprocess_input)
        
        valid_loader = get_dataloader(
            cfg.data_dir, 
            all_ids, 
            'valid', 
            valid_ids,
            cfg.batch_size, 
            cfg.num_workers,
            valid_augmentation, 
            valid_preprocessing)
        log.info(f'len(valid loader) = {len(valid_loader)}')

        # model ########################
        model = factory.get_model(cfg.model)
        model.to(device)

        # optimizer ####################
        optimizer = factory.get_optimizer(model, cfg)

        # initial 
        best = {
            'loss': float('inf'),
            'score': 0.0,
            'epoch': -1,
        }

        # resume model
        if cfg.resume_from:
            log.info('\n')
            log.info(f're-load model from {cfg.resume_from}')
            detail = util.load_model(cfg.resume_from, model, optim, cfg.device)
            best.update({
                'loss': detail['loss'],
                'score': detail['score'],
                'epoch': detail['epoch'],
            })

        # scheduler ####################
        scheduler = factory.get_scheduler(cfg, optimizer, best['epoch'])

        # criterion ####################
        criterion = factory.get_criterion(cfg)

        # tensorboard logs
        # TODO: add tensorboard log
        
        start_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        log.info('\n')
        log.info(f'** start train [fold{fold}th] {start_time} **\n')
        log.info('epoch    iter      rate     | smooth_loss/score | valid_loss/score | best_epoch/best_score |  min')
        log.info('-------------------------------------------------------------------------------------------------')
        
        for epoch in range(start_epoch+1, cfg.epoch):
            end = time.time()
            # set random seed for each epoch
            util.set_global_seed(epoch)

            
            train_result = do_train(model, train_loader, optimizer, criterion, epoch, cfg)
            with torch.no_grad():
                valid_result = do_valid(model, valid_loader, criterion, cfg)
            
            # decay lr if valid_loss doesn't decrease
            scheduler.step(valid_result['loss'])

            detail = {
                'score': valid_result['score'],
                'loss': valid_result['loss'],
                'epoch': epoch,
            }

            # remember best loss and save checkpoint
            if valid_result['score'] >= best['score']:
                best.update(detail)
                util.save_model(model, optimizer, detail, fold, os.path.join(cfg.workdir, 'checkpoint'))


            log.info('%5.1f   %5d    %0.6f   |  %0.4f  %0.4f  |  %0.4f  %6.4f |  %6.1f     %6.4f    | %3.1f min' % \
                (epoch, len(train_loader), util.get_lr(optimizer), train_result['loss'], train_result['score'], valid_result['loss'], valid_result['score'], best['epoch'], best['score'], (time.time() - end) / 60))

            if cfg.early_stop:
                if epoch - best['epoch'] > cfg.early_stop:
                    log.info(f'=================================> early stopping!')
                    break

            time.sleep(0.01)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print(KeyboardInterrupt)
