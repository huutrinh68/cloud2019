from code.utils.logger import log

import os 
import random
import numpy as np 
import torch 

# create seed random
def set_global_seed(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True


def save_model(model, optim, detail, fold, dirname):
    # path = os.path.join(dirname, 'fold%d_ep%d.pt' % (fold, detail['epoch']))
    os.makedirs(os.path.join(dirname, str(fold)), exist_ok=True)
    path = os.path.join(dirname, str(fold), 'top1.pth')
    torch.save({
        'model': model.state_dict(),
        'optim': optim.state_dict(),
        'detail': detail,
    }, path)



def load_model(path, model, optim=None, device=None):
    if device:
        # ramap to device
        state = torch.load(str(path), map_location=device)
    else:
        # remap everthing onto CPU 
        state = torch.load(str(path), map_location=lambda storage, location: storage)

    # model.load_state_dict(state['model'])
    model.load_state_dict(state['state_dict'])
    if optim:
        log.info('loading optim too')
        optim.load_state_dict(state['optim'])
    else:
        log.info('not loading optim')

    model.to(device)

    # detail = state['detail']
    detail = None
    log.info('loaded model from %s' % path)
    return detail


# lr
def get_lr(optim):
    if optim:
        return optim.param_groups[0]['lr']
    else:
        return 0


# evaluate meters
class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count