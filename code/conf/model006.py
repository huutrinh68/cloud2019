workdir = './model006_unet_resnet152'
seed = 100
data_dir = './input/understanding_cloud_organization'
mode = 'train'

batch_size = 10
num_workers = 10
epoch = 300
accumulate_step = 10
early_stop = 10
cutmix_prob = None
mixup_alpha = None

resume_from = None

model = dict(
    architecture='Unet',
    params=dict(
        encoder_name='resnet152',
        encoder_weights='imagenet',
        classes=4,
        activation='sigmoid',
    ),
)

optimizer = dict(
    name='Adam',
    params=dict(
        encoder_lr=5e-4,
        decoder_lr=5e-3,
    ),
)

scheduler = dict(
    name='ReduceLROnPlateau',
    params=dict(
        factor=0.75,
        patience=2,
    ),
)

loss = dict(
    name='BCEDiceLoss',
    params=dict(
        eps=1.0,
    ),
)