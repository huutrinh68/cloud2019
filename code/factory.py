
# -*- coding: utf-8 -*-
from code.utils.logger import log

import torch.nn as nn
import torch
from torch.nn import functional as F
from torchvision import models
import torchvision
import pretrainedmodels
import segmentation_models_pytorch as smp

from torch.optim import lr_scheduler

# model
def get_model(cfg):
    encoder_name = cfg.params.encoder_name
    encoder_weights = cfg.params.encoder_weights
    activation = cfg.params.activation
    classes = cfg.params.classes

    model = getattr(smp, cfg.architecture)(encoder_name=encoder_name, 
                    encoder_weights=encoder_weights,
                    classes=classes,
                    activation=activation)

    log.info('\n')
    log.info('** model setting **')
    log.info(f'loaded model arch = {cfg.architecture}')
    log.info(f'loaded model name = {cfg.params.encoder_name}')
    log.info(f'weight            = {cfg.params.encoder_weights}')
    log.info(f'classes           = {cfg.params.classes}')
    log.info(f'activation        = {cfg.params.activation}')

    return model


# optimizer
def get_optimizer(model, cfg):
    optimizer = getattr(torch.optim, cfg.optimizer.name)([
                {'params': model.encoder.parameters(), 'lr': cfg.optimizer.params.encoder_lr},
                {'params': model.decoder.parameters(), 'lr': cfg.optimizer.params.decoder_lr}, 
            ])
    log.info('\n')
    log.info('** optimizer setting **')
    log.info(f'optimizdr: {optimizer}')

    return optimizer


# scheduler
def get_scheduler(cfg, optim, last_epoch):

    if cfg.scheduler.name == 'ReduceLROnPlateau':
        scheduler = lr_scheduler.ReduceLROnPlateau(
            optim,
            **cfg.scheduler.params,
        )
        scheduler.last_epoch = last_epoch
    else:
        scheduler = getattr(lr_scheduler, cfg.scheduler.name)(
            optim,
            last_epoch=last_epoch,
            **cfg.scheduler.params,
        )
    log.info('\n')
    log.info('** scheduler setting **')
    log.info(f'scheduler: {cfg.scheduler}')

    return scheduler


# criterion
def get_criterion(cfg):
    if cfg.loss.name == 'BCELoss':
        loss = getattr(torch.nn, cfg.loss.name)(**cfg.loss.params)
    elif cfg.loss.name == 'BCEDiceLoss':
        loss = getattr(smp.utils.losses, cfg.loss.name)(**cfg.loss.params)
    elif cfg.loss.name == 'ComboLoss':
        loss = ComboLoss(**cfg.loss.params)
    log.info('\n')
    log.info('** criterion setting **')
    log.info(f'criterion: {loss}')

    return loss