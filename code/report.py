import os
import sys
import torch
from utils import report_checkpoint, Logger



paths =[
    # fold0
    'bin/runs/resnet152/resnet152_augmentadd_OpticalDistortion_fold0/checkpoints/top1.pth',
    # fold1
    'bin/runs/resnet152/resnet152_augmentadd_OpticalDistortion_fold1/checkpoints/top1.pth',
    # fold2
    'bin/runs/resnet152/resnet152_augmentadd_OpticalDistortion_fold2/checkpoints/top1.pth',
    # fold3
    'bin/runs/resnet152/resnet152_augmentadd_OpticalDistortion_fold3/checkpoints/top1.pth',
    # fold4
    'bin/runs/resnet152/resnet152_augmentadd_OpticalDistortion_fold4/checkpoints/top1.pth',
    # fold5
    'bin/runs/resnet152/resnet152_augmentadd_OpticalDistortion_fold5/checkpoints/top1.pth',
    # fold6
    'bin/runs/resnet152/resnet152_augmentadd_OpticalDistortion_fold6/checkpoints/top1.pth',
    # fold7
    'bin/runs/resnet152/resnet152_augmentadd_OpticalDistortion_fold7/checkpoints/top1.pth',
    # fold8
    'bin/runs/resnet152/resnet152_augmentadd_OpticalDistortion_fold8/checkpoints/top1.pth',
    # fold9
    'bin/runs/resnet152/resnet152_augmentadd_OpticalDistortion_fold9/checkpoints/top1.pth',
]

###########################################################################################
if __name__ == "__main__":
    log = Logger()
    log.open(f'checkpoint_report.log', mode='a')

    for path in paths:
        if os.path.exists(path):
            checkpoint = torch.load(path)
            report_checkpoint(checkpoint, log)
            log.write(f"Model_fold    :{path.split('/')[-3]}\n")
        else:
            log.write(f'\n***{path}**** not found!\n')
            sys.exit(1)
