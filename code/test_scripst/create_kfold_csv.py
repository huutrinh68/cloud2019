import os
import pandas as pd

from collections import defaultdict

import pandas as pd
import numpy as np
from numpy.random.mtrand import RandomState

from sklearn.model_selection import StratifiedKFold
from utils import stratified_group_k_fold

if __name__ == "__main__":
    data_dir = '/Users/huutrinh/Documents/cloud2019/input/understanding_cloud_organization/train.csv'
    total_df = pd.read_csv(data_dir)
    total_df['exists'] = total_df['EncodedPixels'].notnull().astype(int)
    total_df['image_name'] = total_df['Image_Label'].map(
        lambda x: x.split('_')[0].strip()
    )
    total_df['class_id'] = total_df['Image_Label'].map(
        lambda x: str(x.split('_')[-1])
    )
    total_df['class_id'] = [
        row.class_id if row.exists else 0 for row in total_df.itertuples()
    ]
    total_df['fold'] = stratified_group_k_fold(
        label='class_id', group_column='image_name', df=total_df, n_splits=5
    )
    # total_df.to_csv('fold.csv', index=False)

    # kfolds
    folds = []
    folds_num = 5
    for i in range(folds_num):
        folds.append(total_df[total_df['fold'] == i])

    for fold_th in range(folds_num):
        # train images id
        train_image = np.setdiff1d(total_df['Image_Label'], total_df[total_df['fold']==fold_th]['Image_Label'])

        # train df
        train_df = total_df[total_df['Image_Label'].isin(train_image)]
        # train_df = total_df[total_df['fold']!=0]

        valid_df = total_df[~total_df['Image_Label'].isin(train_image)]
        # valid_df = total_df[total_df['fold']==0]

        save_dir = '../input/folds'
        os.makedirs(save_dir, exist_ok=True)

        print(f"create fold {fold_th}")
        train_df.to_csv(f'{save_dir}/train_fold{fold_th}.csv', index=False)
        valid_df.to_csv(f'{save_dir}/valid_fold{fold_th}.csv', index=False)