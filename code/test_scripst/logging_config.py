# coding:utf-8
import sys
import logging
import logging.handlers
import logging.config

#create logger, set log level
_root_logger = logging.getLogger('')
_root_logger.setLevel(logging.DEBUG)

#create format(time、loglevel、module_name、function_name、line_number: message)
_simpleFormatter = logging.Formatter(
    fmt='%(asctime)s %(levelname)-8s %(module)-18s %(funcName)-10s %(lineno)4s: %(message)s'
)

#create console handler。output standard, loglevel=DEBUG with above format
_consoleHandler = logging.StreamHandler(sys.stdout)
_consoleHandler.setLevel(logging.DEBUG)
_consoleHandler.setFormatter(_simpleFormatter)

#add consoleHandle to hanlder
_root_logger.addHandler(_consoleHandler)

#create filehandler. filename is logging.log, loglevel=INFO, filesize=1MB, backupfiles=3、encoding=utf-8
_fileHandler = logging.handlers.RotatingFileHandler(
    filename="log", maxBytes=1000000, backupCount=3, encoding='utf-8'
)
_fileHandler.setLevel(logging.INFO)
_fileHandler.setFormatter(_simpleFormatter)

#add filehandler to root logger
_root_logger.addHandler(_fileHandler)