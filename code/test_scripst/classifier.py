import pandas as pd
import numpy as np
submission = pd.read_csv('submission.csv')
sub_class = pd.read_csv('submission_segmentation_and_classifier.csv')
print("submission")
print(submission.head)
print("sub_class")
print(sub_class.head)

predictions_nonempty = set(sub_class.loc[sub_class['EncodedPixels'].isnull(), 'Image_Label'].values)
print(predictions_nonempty)
submission.loc[submission['Image_Label'].isin(predictions_nonempty), 'EncodedPixels'] = np.nan
submission.to_csv('submission_new.csv', index=None)
