workdir = './model004'
seed = 2016
apex = False

n_fold = 15
epoch =300
resume_from = None

batch_size = 22
num_workers = 10
imgsize = (320, 640) #(height, width)
early_stop = 15

loss = dict(
    name='BCEDiceLoss',
    params=dict(
        eps=1.0,
    ),
)

model = dict(
    architecture='Unet',
    params=dict(
        encoder_name='resnet152',
        encoder_weights='imagenet',
        classes=4,
        activation='sigmoid',
        # decoder_use_batchnorm=True,
        # decoder_channels=(256, 128, 64, 32, 16),
        # attention_type='scse',
    ),
)

optim = dict(
    name='Adam',
    params=dict(
        encoder_lr=5e-4,
        decoder_lr=5e-3,
    ),
)


# # scheduler = dict(
# #     name='MultiStepLR',
# #     params=dict(
# #         milestones=[1,2],
# #         gamma=2/3,
# #     ),
# # )

scheduler = dict(
    name='ReduceLROnPlateau',
    params=dict(
        factor=0.75,
        patience=3,
    ),
)

#normalize = {'mean': [0.485, 0.456, 0.406], 'std': [0.229, 0.224, 0.225],}
normalize = None

crop = dict(name='RandomResizedCrop', params=dict(height=imgsize[0], width=imgsize[1], scale=(0.7,1.0), p=1.0))
resize = dict(name='Resize', params=dict(height=imgsize[0], width=imgsize[1]))
hflip = dict(name='HorizontalFlip', params=dict(p=1.0,))
vflip = dict(name='VerticalFlip', params=dict(p=1.0,))
grid_distor = dict(name='GridDistortion', params=dict(p=1.0))
optic_distor = dict(name='OpticalDistortion', params=dict(p=1.0))
contrast = dict(name='RandomBrightnessContrast', params=dict(brightness_limit=0.08, contrast_limit=0.08, p=0.5))
totensor = dict(name='ToTensor', params=dict(normalize=normalize))
rotate = dict(name='Rotate', params=dict(limit=30, border_mode=0), p=0.7)

window_policy = 2

data = dict(
    train=dict(
        model=model,
        dataset_type='CloudDataset',
        dataframe='./input/understanding_cloud_organization/train.csv',
        annotations='./.cache/train_folds.pkl',
        imgdir='./input/understanding_cloud_organization/train_images',
        imgsize=imgsize,
        n_grad_acc=10,
        loss=loss,
        loader=dict(
            shuffle=True,
            batch_size=batch_size,
            drop_last=False,
            num_workers=num_workers,
            pin_memory=False,
        ),
        # remove totensor, this will be added after preprocessing
        # transforms=[crop, hflip, rotate, contrast, totensor], 
        # transforms=[crop, hflip, vflip, grid_distor, optic_distor, rotate, contrast],
        transforms=[resize, vflip, hflip, grid_distor, optic_distor],
        dataset_policy='all',
        window_policy=window_policy,
    ),
    valid = dict(
        model=model,
        dataset_type='CloudDataset',
        dataframe='./input/understanding_cloud_organization/train.csv',
        annotations='./.cache/train_folds.pkl',
        imgdir='./input/understanding_cloud_organization/train_images',
        imgsize=imgsize,
        n_grad_acc=10,
        loss=loss,
        loader=dict(
            shuffle=False,
            batch_size=batch_size,
            drop_last=False,
            num_workers=num_workers,
            pin_memory=False,
        ),
        # remove totensor, this will be added after preprocessing
        # transforms=[crop, hflip, rotate, contrast, totensor],
        # transforms=[crop, hflip, vflip, grid_distor, optic_distor, rotate, contrast],
        transforms=[resize],
        dataset_policy='all',
        window_policy=window_policy,
    ),
    test = dict(
        model=model,
        dataset_type='CloudDataset',
        dataframe='./input/understanding_cloud_organization/sample_submission.csv',
        annotations='./.cache/test.pkl',
        imgdir='./input/understanding_cloud_organization/test_images',
        imgsize=imgsize,
        loader=dict(
            shuffle=False,
            batch_size=8,
            drop_last=False,
            num_workers=num_workers,
            pin_memory=False,
        ),
        # remove totensor, this will be added after preprocessing
        # transforms=[crop, hflip, rotate, contrast, totensor],
        # transforms=[crop, hflip, grid_distor, optic_distor, rotate, contrast],
        transforms=[resize],
        dataset_policy='all',
        window_policy=window_policy,
    ),
)


##test-------------------------------------------
model_1 = dict(
    architecture='Unet',
    params=dict(
        encoder_name='resnet152',
        encoder_weights='imagenet',
        classes=4,
        activation='sigmoid',
    ),
)

model_2 = dict(
    architecture='FPN',
    params=dict(
        encoder_name='resnet152',
        encoder_weights='imagenet',
        classes=4,
        activation='sigmoid',
    ),
)

classify_each_class=1
