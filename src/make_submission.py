# common library --------------------------------
from src.utils.logger import logger, log
logger.setup('./logs', name='log_submission')

from src.utils.include import *
from src.utils.common import *
from src.utils.config import *
import src.utils.util as util
from src.utils.file import *
import src.metrics as metrics

# add library if you want -----------------------
import src.factory as factory

#
# common setting --------------------------------
log.info(COMMON_STRING)
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES']='0,1,2,3'
device = torch.device(f'cuda' if torch.cuda.is_available() else 'cpu')
log.info(f'@{os.path.basename(__file__)}')


model_path=[
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold0/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold1/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold2/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold3/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold4/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold5/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold6/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold7/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold8/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold9/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold10/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold11/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold12/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold13/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_contrast_0658/resnet152_15folds_unet_contrast_fold14/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold0/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold1/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold2/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold3/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold4/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold5/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold6/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold7/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold8/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold9/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold10/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold11/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold12/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold13/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/fpn_0658/resnet152_15folds_retrain_fold14/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold0/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold1/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold2/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold3/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold4/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold5/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold6/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold7/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold8/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold9/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold10/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold11/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold12/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold13/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/unet_0663/resnet152_15folds_retrain_fold14/checkpoints/top1.pth',

    # 'code/bin/runs/resnet152/resnet152_15folds_unet_contrast_test_dice_fold0/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/resnet152_15folds_unet_contrast_test_dice_fold1/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/resnet152_15folds_unet_contrast_test_dice_fold2/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/resnet152_15folds_unet_contrast_test_dice_fold3/checkpoints/top1.pth',
    # 'code/bin/runs/resnet152/resnet152_15folds_unet_contrast_test_dice_fold4/checkpoints/top1.pth',
    'model002/checkpoint/0/top1.pth',
    'model002/checkpoint/1/top1.pth',
    'model002/checkpoint/2/top1.pth',
    'model002/checkpoint/3/top1.pth',
    'model002/checkpoint/4/top1.pth',
    'model002/checkpoint/5/top1.pth',
    'model002/checkpoint/6/top1.pth',
    'model002/checkpoint/7/top1.pth',
    'model002/checkpoint/8/top1.pth',
    'model002/checkpoint/9/top1.pth',
    'model002/checkpoint/10/top1.pth',
    'model002/checkpoint/11/top1.pth',
    'model002/checkpoint/12/top1.pth',
    'model002/checkpoint/13/top1.pth',
    'model002/checkpoint/14/top1.pth',
    ]


sigmoid = lambda x: 1 / (1 + np.exp(-x))


# post processing to remove tiny mask
def post_process(probability, threshold, min_size):
    """
    Post processing of each predicted mask, components with lesser number of pixels
    than `min_size` are ignored
    """
    # don't remember where I saw it
    mask = cv2.threshold(probability, threshold, 1, cv2.THRESH_BINARY)[1]
    num_component, component = cv2.connectedComponents(mask.astype(np.uint8))
    predictions = np.zeros((350, 525), np.float32)
    num = 0
    for c in range(1, num_component):
        p = (component == c)
        if p.sum() > min_size:
            predictions[p] = 1
            num += 1
    return predictions, num



# mask to run length encode
def mask2rle(img):
    '''
    Convert mask to rle.
    img: numpy array, 1 - mask, 0 - background
    Returns run length as string formated
    '''
    pixels= img.T.flatten()
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[::2]
    return ' '.join(str(x) for x in runs)




#test time augmentation  -----------------------
def null_augment   (input): return input
def flip_lr_augment(input): return torch.flip(input, dims=[2])
def flip_ud_augment(input): return torch.flip(input, dims=[3])

def null_inverse_augment   (logit): return logit
def flip_lr_inverse_augment(logit): return torch.flip(logit, dims=[2])
def flip_ud_inverse_augment(logit): return torch.flip(logit, dims=[3])

augment = (
    (null_augment,   null_inverse_augment   ),
    (flip_lr_augment,flip_lr_inverse_augment),
    (flip_ud_augment,flip_ud_inverse_augment),
)


# load_checkpoint -------------------------------
def load_checkpoint(path, cfg, i):

    # if 'runs/resnet152/unet' or 'runs/resnet152/resnet152' in path:
    #     model = factory.get_model(cfg.model_1)
    # elif 'runs/resnet152/fpn' in path:
    #     model = factory.get_model(cfg.model_2)
    # elif 'model002' in path:

    model = factory.get_model(cfg.model_3)
    
    try:
        if len(cfg.gpu) > 1:
            if i%2 == 0:
                device = f'cuda:{cfg.gpu[0]}'
            else:
                device = f'cuda:{cfg.gpu[1]}'
        else:
            device = cfg.device

        checkpoint = torch.load(path, map_location=device)
        util.report_checkpoint(checkpoint)

    except FileNotFoundError as err:
        log.error(err)
        sys.exit(1)


    log.info(f'device: {device}')
    model.load_state_dict(checkpoint['model'])
    # model.load_state_dict(checkpoint['state_dict'])
    
    model.to(device)
    model.eval()

    return model



# get args from command line --------------------
def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument('config')
    parser.add_argument('--gpu', nargs='+', type=int)
    parser.add_argument('--output')
    parser.add_argument('--n_tta')
    
    return parser.parse_args()


# main-------------------------------------------
def main():
    args = get_args()
    cfg  = Config.fromfile(args.config)

    # copy command line args to cfg
    cfg.output = args.output
    cfg.gpu = args.gpu
    global device
    cfg.device = device
    

    class_params = {}
    if cfg.classify_each_class:
        class_params[0] =  (0.6, 20000)
        class_params[1] =  (0.6, 20000)
        class_params[2] =  (0.6, 22500)
        class_params[3] =  (0.6, 15000)
        log.info(f'(threshold, minsize): {class_params}')
    else:
        threshold = 0.6
        minsize = 20000
    

    models = []
    for i, path in enumerate(model_path):
        models.append(load_checkpoint(path, cfg, i))
        log.info(path)
    log.info(f'n_models = {len(models)}')

    
    loader_test = factory.get_dataloader(cfg.data.test)
    log.info(f'len(loader_test): {len(loader_test)}')

    encoded_pixels = []
    torch.cuda.empty_cache()

    for i, test_batch in enumerate(tqdm(loader_test)):
        
        if len(models) > 1:
            for m, model in enumerate(models):
                if len(cfg.gpu) > 1:
                    if m%2 == 0:
                        device = f'cuda:{cfg.gpu[0]}'
                    else:
                        device = f'cuda:{cfg.gpu[1]}'
                
                ### tta
                if len(augment) > 1:
                    for k, (a, inv_a) in enumerate(augment):
                        logit = model(a(test_batch[0].to(device)))
                        p = inv_a(torch.sigmoid(logit))
                        if k == 0:
                            prob  = 0.8*p
                        else:
                            prob += 0.1*p
                        
                        outputs = prob.cpu().detach().numpy()

                    del prob
                    gc.collect()
                else:
                    outputs = torch.sigmoid(model(test_batch[0].to(device)))
                    outputs = outputs.cpu().detach().numpy()
                ########

                # ensemble outputs of all model
                if m == 0:
                    outputs_all = outputs
                else:
                    outputs_all += outputs
                
            preds = outputs_all / len(models)

            del outputs_all, outputs, model
            gc.collect()
            torch.cuda.empty_cache()

        elif len(models) == 1:
            if len(augment) > 1:
                for k, (a, inv_a) in enumerate(augment):
                    logit = model(a(test_batch[0].to(device)))
                    p = inv_a(torch.sigmoid(logit))
                    if k == 0:
                        prob = 0.8*p
                    else:
                        prob += 0.1*p
                    
                    outputs = prob.cpu().detach().numpy()

                del prob
                gc.collect()
            else:
                    outputs = torch.sigmoid(model(test_batch[0].to(device)))
                    outputs = outputs.cpu().detach().numpy()
            
            preds = outputs

            del outputs, model
            gc.collect()
            torch.cuda.empty_cache()

        else:
            log.error('Not found any model!')
            exit(0)

        for i, batch in enumerate(preds):
            for j, prob in enumerate(batch):
                if prob.shape != (350, 525):
                    prob = cv2.resize(prob, dsize=(525, 350), interpolation=cv2.INTER_CUBIC)
                
                if cfg.classify_each_class:
                    predict, num_predict = post_process(sigmoid(prob), class_params[j%4][0], class_params[j%4][1])
                else:
                    predict, num_predict = post_process(sigmoid(prob), threshold, minsize)

                if num_predict == 0:
                    encoded_pixels.append('')
                else:
                    r = mask2rle(predict)
                    encoded_pixels.append(r)

                del prob, predict
                gc.collect()
                torch.cuda.empty_cache()

            del batch
            gc.collect()
            torch.cuda.empty_cache()

        del test_batch
        gc.collect()
        torch.cuda.empty_cache()
    
    
    sub = pd.read_csv(cfg.data.test.dataframe)
    log.info(f'len(test) = {len(sub)}\n')
    sub['EncodedPixels'] = encoded_pixels
    sub.to_csv(cfg.output, columns=['Image_Label', 'EncodedPixels'], index=False)

    log.info(f'{sub.head()}')
    log.info(f'success!')


##
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print('Keyboard Interrupted')