import torch 
import numpy as np

#https://blog.shikoan.com/pytorch-onehotencoding/
#https://github.com/eladhoffer/utils.pytorch/blob/master/cross_entropy.py
def onehot(indexs, N=None, ignore_index=None):
    """
    Create a one-representation of indexs with N possible entries
    if N is not specified, it will suit the maximum index appearing.
    indexs is a long-tensor of indexs
    ignore_index will be zero in onehot representation
    """
    if N is None:
        N=indexs.max()+1
    
    sz = list(indexs.size())
    output = indexs.new().byte().resize_(*sz, N).zero_()
    output.scatter_(-1, indexs.unsqueeze(-1), 1)

    if ignore_index is not None and ignore_index >= 0:
        output.masked_fill_(indexs.eq(ignore_index).unsqueeze(-1), 0)
    return output

#https://github.com/pytorch/pytorch/issues/7455
def smooth_one_hot(true_labels: torch.Tensor, classes: int, smoothing=0.0):
    """
    if smoothing == 0, it's one-hot method
    if 0 < smoothing < 1, it's smooth method

    """
    assert 0.0 <= smoothing < 1.0
    confidence = 1.0 - smoothing
    label_shape = torch.Size((true_labels.size(0), classes))
    with torch.no_grad():
        true_dist = torch.empty(size=label_shape, device=true_labels.device)
        true_dist.fill_(smoothing / (classes - 1))
        true_dist.scatter_(1, true_labels.data.unsqueeze(1), confidence)
    return true_dist


if __name__ == "__main__":

    for i in range(4):
        x = torch.tensor([i])
        result = smooth_one_hot(x, classes=4, smoothing=0.2)
        print(torch.eye(4)[x])
        print(result)