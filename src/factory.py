# common library --------------------------------
from src.utils.logger import log
from src.utils.include import *

# import other ----------------------------------
from src.dataset.dataset import CloudDataset
from src.dataset.transforms import RandomResizedCrop
from src.losses.losses import ComboLoss

# model 
def get_model(cfg):
    log.info('\n')
    log.info('** model setting **')
    log.info(f'model: {cfg.architecture}')
    log.info(f'encoder_name: {cfg.params.encoder_name}')
    log.info(f'encoder_weights: {cfg.params.encoder_weights}')
    log.info(f'classes: {cfg.params.classes}')
    log.info(f'activation: {cfg.params.activation}')

    model = getattr(smp, cfg.architecture)(**cfg.params)

    return model


# criterion
def get_loss(cfg):
    if cfg.loss.name == 'BCELoss':
        loss = getattr(torch.nn, cfg.loss.name)(**cfg.loss.params)
    elif cfg.loss.name == 'BCEDiceLoss':
        loss = getattr(smp.utils.losses, cfg.loss.name)(**cfg.loss.params)
    elif cfg.loss.name == 'ComboLoss':
        loss = ComboLoss(**cfg.loss.params)
    log.info('\n')
    log.info('** criterion setting **')
    log.info(f'criterion: {loss}')

    return loss


# optim
def get_optim(cfg, model):

    optim = getattr(torch.optim, cfg.optim.name)([
                {'params': model.encoder.parameters(), 'lr': cfg.optim.params.encoder_lr},
                {'params': model.decoder.parameters(), 'lr': cfg.optim.params.decoder_lr}, 
            ])
    log.info('\n')
    log.info('** optim setting **')
    log.info(f'optimizdr: {optim}')

    return optim


# scheduler
def get_scheduler(cfg, optim, last_epoch):

    if cfg.scheduler.name == 'ReduceLROnPlateau':
        scheduler = lr_scheduler.ReduceLROnPlateau(
            optim,
            **cfg.scheduler.params,
        )
        scheduler.last_epoch = last_epoch
    else:
        scheduler = getattr(lr_scheduler, cfg.scheduler.name)(
            optim,
            last_epoch=last_epoch,
            **cfg.scheduler.params,
        )
    log.info('\n')
    log.info('** scheduler setting **')
    log.info(f'scheduler: {cfg.scheduler}')

    return scheduler

# dataloader
def get_dataloader(cfg, folds=None):

    dataset = CloudDataset(cfg, folds)
    loader = DataLoader(dataset, **cfg.loader)

    return loader


# transform
def get_transforms(cfg):

    def get_object(transform):
        if hasattr(A, transform.name):
            return getattr(A, transform.name)
        else:
            return eval(transform.name)
    
    transforms = [get_object(transform)(**transform.params) for transform in cfg.transforms]
    log.info(f'transforms: {cfg.transforms}')

    return A.Compose(transforms)


# get preprocessing
def get_preprocessing(cfg): 
    # convert to tensor then move channel to the first
    def to_tensor(x, **kwargs):
        return x.transpose(2, 0, 1).astype('float32')

    preprocessing_fn = smp.encoders.get_preprocessing_fn(
        cfg.model.params.encoder_name, 
        cfg.model.params.encoder_weights
        )

    _transform = [
        A.Lambda(image=preprocessing_fn),
        A.Lambda(image=to_tensor, mask=to_tensor),
    ]
    
    return A.Compose(_transform)
