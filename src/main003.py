# common library --------------------------------
from src.utils.logger import logger, log
logger.setup('./logs', name='log_model003_unetresnet152')

from src.utils.include import *
from src.utils.common import *
from src.utils.config import *
import src.utils.util as util
from src.utils.file import *
import src.metrics as metrics


# add library if you want -----------------------
import src.factory as factory

#
# common setting --------------------------------
log.info(COMMON_STRING)
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES']='0,1,2,3'
device = torch.device(f'cuda' if torch.cuda.is_available() else 'cpu')
log.info(f'@{os.path.basename(__file__)}')


# get args from command line --------------------
def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument('mode', choices=['train', 'valid', 'test'])
    parser.add_argument('config')
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--fold', type=int, required=True)
    parser.add_argument('--gpu', nargs='+', type=int)
    parser.add_argument('--snapshot')
    parser.add_argument('--output') 
    parser.add_argument('--n_tta', default=1, type=int)
    
    return parser.parse_args()



# show config -----------------------------------
def show_config(cfg):
    log.info('---[START %s] %s'%(IDENTIFIER, '-'*32))
    log.info('\n')
    log.info('** show config **')
    log.info(f'workdir:     {cfg.workdir}')
    log.info(f'logpath:     {logger.path}')
    log.info(f'seed:        {cfg.seed}')
    log.info(f'model:       {cfg.model}')
    log.info(f'optim:       {cfg.optim}')
    log.info(f'loss:        {cfg.loss}')
    log.info(f'scheduler:   {cfg.scheduler}')
    log.info(f'mode:        {cfg.mode}')
    log.info(f'fold:        {cfg.fold}')
    log.info(f'epoch:       {cfg.epoch}')
    log.info(f'batch size:  {cfg.batch_size}')
    log.info(f'acc:         {cfg.data.train.n_grad_acc}')
    log.info(f'n_workers:   {cfg.num_workers}')
    log.info(f'apex:        {cfg.apex}')
    log.info(f'imgsize:     {cfg.imgsize}')
    log.info(f'normalize:   {cfg.normalize}')

    log.info(f'debug:       {cfg.debug}')
    log.info(f'n_tta:       {cfg.n_tta}')
    log.info(f'resume_from: {cfg.resume_from}')
    log.info(f'early_stop:  {cfg.early_stop}')
    
    # device
    log.info(f'gpu:         {cfg.gpu}')
    log.info(f'device:      {cfg.device}')
    log.info('\n')



# train model -----------------------------------
def do_train(cfg, model):
    # get criterion -----------------------------
    criterion = factory.get_loss(cfg)

    # get optimization --------------------------
    optim = factory.get_optim(cfg, model)

    # initial -----------------------------------
    best = {
        'loss': float('inf'),
        'score': 0.0,
        'epoch': -1,
    }

    # resume model ------------------------------
    if cfg.resume_from:
        log.info('\n')
        log.info(f're-load model from {cfg.resume_from}')
        detail = util.load_model(cfg.resume_from, model, optim, cfg.device)
        best.update({
            'loss': detail['loss'],
            'score': detail['score'],
            'epoch': detail['epoch'],
        })

    # scheduler ---------------------------------
    scheduler = factory.get_scheduler(cfg, optim, best['epoch'])

    # fp16 --------------------------------------
    if cfg.apex:
        amp.initialize(model, optim, opt_level='O1', verbosity=0)


    # setting dataset ---------------------------
    log.info('\n')
    log.info('** dataset setting **')
    folds = [fold for fold in range(cfg.n_fold) if cfg.fold != fold]
    log.info(f'fold_train:    {folds}')
    log.info(f'fold_valid:    [{cfg.fold}]')

    loader_train = factory.get_dataloader(cfg.data.train, folds)
    loader_valid = factory.get_dataloader(cfg.data.valid, [cfg.fold])
    log.info(f'len(loader_train): {len(loader_train)}')
    log.info(f'len(loader_valid): {len(loader_valid)}')


    # start trainging ---------------------------
    start_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    log.info('\n')
    log.info(f'** start train [fold{cfg.fold}th] {start_time} **\n')
    log.info('epoch    iter      rate     | smooth_loss/score | valid_loss/score | best_epoch/best_score |  min')
    log.info('-------------------------------------------------------------------------------------------------')
    
    for epoch in range(best['epoch']+1, cfg.epoch):
        end = time.time()
        util.set_seed(epoch)

        ## train model --------------------------
        train_results = run_nn(cfg.data.train, 'train', model, loader_train, criterion=criterion, optimizer=optim, apex=cfg.apex, epoch=epoch)
        
        ## valid model --------------------------
        with torch.no_grad():
            val_results = run_nn(cfg.data.valid, 'valid', model, loader_valid, criterion=criterion, epoch=epoch)

        detail = {
            'score': val_results['score'],
            'loss': val_results['loss'],
            'epoch': epoch,
        }
        if val_results['loss'] <= best['loss']:
            best.update(detail)
            util.save_model(model, optim, detail, cfg.fold, os.path.join(cfg.workdir, 'checkpoint'))
        
        log.info('%5.1f   %5d    %0.6f   |  %0.4f  %0.4f  |  %0.4f  %6.4f |  %6.1f     %6.4f    | %3.1f min' % \
                (epoch+1, len(loader_train), util.get_lr(optim), train_results['loss'], train_results['score'], val_results['loss'], val_results['score'], best['epoch'], best['score'], (time.time() - end) / 60))
        

        scheduler.step(val_results['loss']) # if scheduler is reducelronplateau
        # scheduler.step()


        # early stopping-------------------------
        if cfg.early_stop:
            if epoch - best['epoch'] > cfg.early_stop:
                log.info(f'=================================> early stopping!')
                break
        time.sleep(0.01)



# valid -----------------------------------------
def do_valid(cfg, model):
    assert cfg.output
    criterion = factory.get_loss(cfg)
    util.load_model(cfg.snapshot, model)
    loader_valid = factory.get_dataloader(cfg.data.valid, [cfg.fold])
    with torch.no_grad():
        results = [run_nn(cfg.data.valid, 'valid', model, loader_valid, criterion=criterion) for i in range(cfg.n_tta)]
    with open(cfg.output, 'wb') as f:
        pickle.dump(results, f)
    log.info('saved to %s' % cfg.output)



# test ------------------------------------------
def do_test(cfg, model):
    assert cfg.output
    util.load_model(cfg.snapshot, model)
    loader_test = factory.get_dataloader(cfg.data.test)
    with torch.no_grad():
        results = [run_nn(cfg.data.test, 'test', model, loader_test) for i in range(cfg.n_tta)]
    with open(cfg.output, 'wb') as f:
        pickle.dump(results, f)
    log.info('saved to %s' % cfg.output)



## run model ------------------------------------
def run_nn(cfg, mode, model, loader, criterion=None, optimizer=None, scheduler=None, apex=None, epoch=None):
    if mode in ['train']:
        model.train()
    elif mode in ['valid', 'test']:
        model.eval()
    else:
        raise 

    losses = util.AverageMeter()
    scores = util.AverageMeter()

    ids_all = []
    targets_all = []
    outputs_all = []

    num_its = len(loader)

    for i, (inputs, targets, ids) in enumerate(loader):
        batch_size = len(inputs)

        # zero out gradients so we can accumulate new ones over batches
        if mode in ['train']:
            optimizer.zero_grad()

        # move data to device
        inputs = inputs.to(device, dtype=torch.float)
        targets = targets.to(device, dtype=torch.float)
        
        outputs = model(inputs)
        # both train mode and valid mode
        if mode in ['train', 'valid']:
            with torch.set_grad_enabled(mode == 'train'):
                loss = criterion(outputs, targets)
                # loss = criterion(torch.sigmoid(outputs), targets)

                loss = loss/cfg.n_grad_acc
                with torch.no_grad():
                    losses.update(loss.item()) # or losses.update(loss.item(), inputs.size(0))

        # train mode
        if mode in ['train']:
            if apex:
                with amp.scale_loss(loss, optimizer) as scaled_loss:
                    scaled_loss.backward()
            else:
                loss.backward() # accumulate loss
            
            if (i+1) % cfg.n_grad_acc == 0 or (i+1) == num_its:
                torch.nn.utils.clip_grad_norm(model.parameters(), 1.0)
                optimizer.step() # update
                optimizer.zero_grad() # flush
        
        # compute metrics
        # score = metrics.mean_dice_coef(outputs, targets)
        score = metrics.dice_score(outputs, targets)
        with torch.no_grad():
            scores.update(score.item())

            if mode in ['test']:
                ids_all.extend(ids)
                targets_all.extend(targets.cpu().numpy())
                outputs_all.extend(torch.sigmoid(outputs).cpu().numpy())
                #outputs_all.append(torch.softmax(outputs, dim=1).cpu().numpy())
            
    result = {
        'loss': losses.avg,
        'score': scores.avg,
        'ids': ids_all,
        'targets': np.array(targets_all),
        'outputs': np.array(outputs_all),
    }

    return result


# main ------------------------------------------
def main():
    args = get_args()
    cfg  = Config.fromfile(args.config)

    # copy command line args to cfg
    cfg.mode = args.mode
    cfg.debug = args.debug
    cfg.fold = args.fold
    cfg.snapshot = args.snapshot
    cfg.output = args.output
    cfg.n_tta = args.n_tta
    cfg.gpu = args.gpu
    global device
    cfg.device = device
    

    # print setting
    show_config(cfg)

    # torch.cuda.set_device(cfg.gpu)
    util.set_seed(cfg.seed)
    log.info(f'setting seed = {cfg.seed}')

    # setup -------------------------------------
    for f in ['checkpoint', 'train', 'valid', 'test', 'backup']: os.makedirs(cfg.workdir+'/'+f, exist_ok=True)
    if 0: #not work perfect
        backup_project_as_zip(PROJECT_PATH, cfg.workdir+'/backup/code.train.%s.zip'%IDENTIFIER)

    ## model ------------------------------------
    model = factory.get_model(cfg.model)

    # multi-gpu----------------------------------
    if torch.cuda.device_count() > 1 and len(cfg.gpu) > 1:
        print("Let's use", torch.cuda.device_count(), "GPUs!")
        model = nn.DataParallel(model)
    # elif str(device) in ['cuda']:# one-gpu
    #     device = f'{device}:{cfg.gpu[0]}'
    #     cfg.device = device # reassign
    model.to(device)

    ## ------------------------------------------
    if cfg.mode == 'train':
        do_train(cfg, model)
    elif cfg.mode == 'valid':
        do_valid(cfg, model)
    elif cfg.mode == 'test':
        do_test(cfg, model)
    else:
        log.error(f"mode '{cfg.mode}' is not in [train, valid, test]")
        exit(0)



##
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print('Keyboard Interrupted')
