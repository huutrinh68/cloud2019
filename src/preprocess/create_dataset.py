# common library --------------------------------
from src.utils.logger import logger, log
logger.setup('./logs', name='log')

from src.utils.include import *
from src.utils.common import *
from src.utils.config import *
from src.utils.util import *
from src.utils.file import *

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input')
    parser.add_argument('--output')
    return parser.parse_args()

def main():
    args = get_args()
    df = pd.read_csv(args.input)
    df['img_id'] = df['Image_Label'].apply(lambda x: x.split('_')[0])

    ids = pd.DataFrame(data=df['img_id'].drop_duplicates().values, columns=['img_id'])
    with open(args.output, 'wb') as f:
        pickle.dump(ids, f)
    ids.to_pickle(args.output)

    print(ids.head())
    print(len(ids))
    print('saved to %s' % args.output)



if __name__ == '__main__':
    print('@%s'%os.path.basename(__file__))
    main()
