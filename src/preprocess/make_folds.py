# common library --------------------------------
from src.utils.logger import logger, log
logger.setup('./logs', name='log_makefolds')

from src.utils.include import *
from src.utils.common import *
from src.utils.config import *
from src.utils.util import *
from src.utils.file import *

# import other ----------------------------------
import src.utils.kfold as kfold

# bad image list
bad_train = ["5e70931.jpg", "7ca1d0b.jpg", "1e40a05.jpg", "8bd81ce.jpg", "41f92e5.jpg", "449b792.jpg", "563fc48.jpg",
            "1588d4c.jpg", "046586a.jpg", "b092cc1.jpg", "c26c635.jpg", "c0306e5.jpg", "e04fea3.jpg", "e5f2f24.jpg",
            "ee0ba55.jpg", "fa645da.jpg", "eda52f2.jpg", "d821c94.jpg", "24884e7.jpg", "d744e88.jpg"]


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input')
    parser.add_argument('--output')
    parser.add_argument('--n-fold', type=int, default=10)
    parser.add_argument('--seed', type=int, default=10)
    return parser.parse_args()


def _make_folds(df, n_fold, seed):

    ## preprocess
    df['label'] = df['Image_Label'].apply(lambda x: x.split('_')[1])
    df['img_id'] = df['Image_Label'].apply(lambda x: x.split('_')[0])
    
    ### remove bad image list from train data
    df = df[~df['img_id'].isin(bad_train)]
    log.info(f'removed {len(bad_train)} bad image\n')

    ###https://www.kaggle.com/c/understanding_cloud_organization/discussion/111731#latest-645372
    folds = df.loc[df['EncodedPixels'].isnull() == False, 'Image_Label'].apply(
        lambda x: x.split('_')[0]).value_counts().reset_index().rename(
            columns={'index': 'img_id', 'Image_Label': 'count'}).sort_values(['count', 'img_id'])

    # add fold information
    folds['fold'] = kfold.stratified_group_k_fold(
        label='count', group_column='img_id', df=folds, n_splits=n_fold
    )

    return folds



def main():
    args = get_args()
    df = pd.read_csv(args.input)

    folds = _make_folds(df, args.n_fold, args.seed)

    with open(args.output, 'wb') as f:
        pickle.dump(folds, f)
    folds.to_pickle(args.output)

    log.info(f'n_folds: {args.n_fold}')
    log.info(f'len(df): {len(folds)}')
    log.info(folds.head())
    log.info('saved to %s' % args.output)



if __name__ == '__main__':
    print('@%s'%os.path.basename(__file__))
    main()
