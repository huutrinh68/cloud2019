import numpy as np 
import torch

def dice_score(predict, target, smooth=1e-5):
    # first convert to sigmoid
    predict = torch.sigmoid(predict)
    smooth = 1.

    pflat = predict.view(-1)
    tflat = target.view(-1)
    pflat = pflat.cpu().detach()
    tflat = tflat.cpu().detach()
    intersection = (pflat * tflat).sum()
    dice_score = (2. * intersection + smooth) / (pflat.sum() + tflat.sum() + smooth)

    return dice_score

#------------------------------------------------
#https://www.kaggle.com/c/understanding_cloud_organization/discussion/114093
#https://www.kaggle.com/wh1tezzz/correct-dice-metrics-for-this-competition
def single_dice_coef(y_true, y_pred_bin):
    # shape of y_true and y_pred_bin: (height, width)
    
    # numpy version -----------------------------
    # y_true = np.asarray(y_true.cpu().detach().numpy()).astype(np.bool)
    # y_pred_bin = np.asarray(y_pred_bin.cpu().detach().numpy()).astype(np.bool)

    # intersection = np.sum(y_true * y_pred_bin)
    # if (np.sum(y_true)==0) and (np.sum(y_pred_bin)==0):
    #     return 1
    # return (2*intersection+esp) / (np.sum(y_true) + np.sum(y_pred_bin)+esp)

    # torch version -----------------------------
    esp         = 1e-9
    threshold   = 0.5
    y_true      = (y_true.view(-1) > 0.5).float()
    y_pred_bin  = (y_pred_bin.view(-1) > threshold).float()

    intersection = (y_true * y_pred_bin).sum()
    if (y_true.sum()==0) and (y_pred_bin.sum()==0):
        return 1
    return (2*intersection+esp) / (y_true.sum() + y_pred_bin.sum()+esp)

def mean_dice_coef(y_true, y_pred_bin):
    # shape of y_true and y_pred_bin: (n_samples, height, width, n_channels)
    batch_size = y_true.shape[0]
    channel_num = y_true.shape[1]
    mean_dice_channel = 0.
    
    # torch version------------------------------
    with torch.no_grad(): 
        for i in range(batch_size):
            for j in range(channel_num):
                channel_dice = single_dice_coef(y_true[i, j, :, :], y_pred_bin[i, j, :, :])
                mean_dice_channel += channel_dice/(channel_num*batch_size)
    
    # numpy version -----------------------------
    # for i in range(batch_size):
    #     for j in range(channel_num):
    #         channel_dice = single_dice_coef(y_true[i, j, :, :], y_pred_bin[i, j, :, :])
    #         mean_dice_channel += channel_dice/(channel_num*batch_size)

    return mean_dice_channel


#https://gist.github.com/JDWarner/6730747
def dice(im1, im2):
    """
    Computes the Dice coefficient, a measure of set similarity.
    Parameters
    ----------
    im1 : array-like, bool
        Any array of arbitrary size. If not boolean, will be converted.
    im2 : array-like, bool
        Any other array of identical size. If not boolean, will be converted.
    Returns
    -------
    dice : float
        Dice coefficient as a float on range [0,1].
        Maximum similarity = 1
        No similarity = 0
    Notes
    -----
    The order of inputs for `dice` is irrelevant. The result will be
    identical if `im1` and `im2` are switched.
    """
    im1 = np.asarray(im1).astype(np.bool)
    im2 = np.asarray(im2).astype(np.bool)

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

    # Compute Dice coefficient
    intersection = np.logical_and(im1, im2)

    return 2. * intersection.sum() / (im1.sum() + im2.sum())


def dice_all(y_true, y_pred):
    scores = []
    y_pred = torch.sigmoid(y_pred) #???

    y_true = y_true.cpu().detach().numpy()
    y_pred = y_pred.cpu().detach().numpy()

    for y_val_, y_pred_ in zip(y_true, y_pred):
        score = dice(y_val_, y_pred_ > 0.5)
        if np.isnan(score):
            scores.append(1)
        else:
            scores.append(score)

    return np.mean(scores)