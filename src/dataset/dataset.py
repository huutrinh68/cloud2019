# common library --------------------------------
from src.utils.logger import log
from src.utils.include import *

# import other ----------------------------------
import src.factory as factory
import src.utils.kfold as kfold


# bad image list --------------------------------
bad_train = ["5e70931.jpg", "7ca1d0b.jpg", "1e40a05.jpg", "8bd81ce.jpg", "41f92e5.jpg", "449b792.jpg", "563fc48.jpg",
            "1588d4c.jpg", "046586a.jpg", "b092cc1.jpg", "c26c635.jpg", "c0306e5.jpg", "e04fea3.jpg", "e5f2f24.jpg",
            "ee0ba55.jpg", "fa645da.jpg", "eda52f2.jpg", "d821c94.jpg", "24884e7.jpg", "d744e88.jpg"]

## apply dataset --------------------------------
def apply_dataset_policy(df, policy):
    if policy == 'all':
        pass
    else:
        raise
    log.info(f'apply_dataset_policy: {policy}')  

    return df


# make masks data, which to be labels -----------
def make_mask(df: pd.DataFrame, image_name: str='img.jpg', shape: tuple = (1400, 2100)):

    encoded_masks = df.loc[df['img_id'] == image_name, 'EncodedPixels']
    masks = np.zeros((shape[0], shape[1], 4), dtype=np.float32)

    for idx, label in enumerate(encoded_masks.values):
        if label is not np.nan:
            mask = rle_decode(label)
            masks[:, :, idx] = mask

    return masks


# run length decode
def rle_decode(mask_rle: str = '', shape: tuple = (1400, 2100)):
    '''
    Decode rle encoded mask.
    
    :param mask_rle: run-length as string formatted (start length)
    :param shape: (height, width) of array to return 
    Returns numpy array, 1 - mask, 0 - background
    '''
    s = mask_rle.split()
    starts, lengths = [np.asarray(x, dtype=int) for x in (s[0:][::2], s[1:][::2])]
    starts -= 1
    ends = starts + lengths
    img = np.zeros(shape[0] * shape[1], dtype=np.uint8)
    for lo, hi in zip(starts, ends):
        img[lo:hi] = 1
    return img.reshape(shape, order='F')


## dataset --------------------------------------
class CloudDataset(torch.utils.data.Dataset):
    def __init__(self, cfg, folds):
        self.cfg = cfg
        self.folds = folds
        self.df = pd.read_csv(cfg.dataframe)
        self.df['img_id'] = self.df['Image_Label'].apply(lambda x: x.split('_')[0])

        self.transforms = factory.get_transforms(self.cfg)
        self.preprocessing = factory.get_preprocessing(self.cfg)

        with open(cfg.annotations, 'rb') as f:
            log.info(f'loaded file: {cfg.annotations}')
            self.df_fold = pickle.load(f)
            
        if folds:
            self.df_fold = self.df_fold[self.df_fold.fold.isin(folds)]

        self.df_fold = apply_dataset_policy(self.df_fold, self.cfg.dataset_policy)

    def __len__(self):
        return len(self.df_fold)
    
    # def __str__(self):
    def __repr__(self):
        str = ''
        str += f'use default(random) sampler\t'
        str += f'folds: {self.folds}\t'
        str += f'len: {len(self.df_fold)}'
        return str

    def __getitem__(self, idx):
        image_name = self.df_fold['img_id'].iloc[idx]
        
        mask = make_mask(self.df, image_name)
        path = os.path.join(self.cfg.imgdir, image_name)
        img = cv2.imread(path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        augmented = self.transforms(image=img, mask=mask)
        img = augmented['image']
        mask = augmented['mask']
        
        if self.preprocessing:
            preprocessed = self.preprocessing(image=img, mask=mask)
            img = preprocessed['image']
            mask = preprocessed['mask']
        return img, mask, image_name
